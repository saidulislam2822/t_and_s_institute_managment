@extends('layout.site')

@section('content')

  <div class="container">
    <div class="row " style="margin-top:50px">
      <div class="col-12">
        <div class="card" style="border: none">
          <div class="card-body">
            <section class="panel">
              <header class="panel-heading border-bottom mb-4 text-center">
                <h2 class="panel-title">Job Fair Title</h2>
              </header>


              <div class="panel-body">

                <div class="card">
                  <div class="card-header text-center">
{{--                    <h4 class="">Job Fair title</h4>--}}

                    <p>.........................</p>
                  </div>
                  <div class="card-body">
                    <ul>
                      <li><strong>Title : </strong> ............</li>
                      <li><strong>Provider : </strong> ...........</li>
                      <li><strong>Start at : </strong> ........</li>
                      <li><strong>End at : </strong> ............</li>
                      <li><strong>Institute Phone : </strong> ............</li>
                      <li><strong>Institute Email : </strong> .....................</li>
                    </ul>
                    <div class="row">
                      <div class="col-md-12 text-center">
                        <a class="btn btn-primary" href="{{ route('jobs') }}">See Job List</a>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
