@extends('layout.site')

@section('stylesheet')
  <style>

      .row .event-main-design-align:hover {
          transform: scale(1.001);
          box-shadow: 0 10px 20px rgba(0, 0, 0, .12), 0 4px 8px rgba(0, 0, 0, .06);
      }
      i{ color: white;}
  </style>

@endsection

@section('content')

  <section id="hero" class="align-items-center">

    <div class="row" style="margin-top: 40px;">
      <div class="col-md-12 text-center">
        <h1 class="justify-content-center" style="font-size: xx-large;;">You Deserve A Job That Loves You Back</h1>
        <h2 class="justify-content-center" >Create An Account Or Sign In</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12" style="padding-left: 35%; padding-right: 35%;">
        {{--          <img src="{{asset('assets/frontend/img/hero-img.png')}}" class="img-fluid animated" alt="">--}}
        <div class="input-group">
          <input type="text" class="form-control rounded" placeholder="Enter Email" aria-label="Search" aria-describedby="search-addon"/>
        </div>
      </div>
      <div class="col-sm-12" style="margin-top: 10px; padding-left: 35%; padding-right: 35%;">
        <button type="button" class="btn btn-success" style="width: 100%; background-color: #008080;">Continue With Email</button>
      </div>
    </div>
{{--    <div class="container">--}}
{{--      <div class="row">--}}
{{--        <div class="col-lg-12 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1"--}}
{{--             data-aos="fade-up" data-aos-delay="200">--}}
{{--          <h1>Job Fair</h1>--}}
{{--        </div>--}}
{{--      </div>--}}
{{--      --}}
{{--    </div>--}}
  </section>
  <div class="container" data-aos="fade-up">
    <div class="section-title mt-5">
      <h2 style="text-align: center">Job Fair List</h2>
    </div>
    <div class="row">
      <div class="col-md-6" style="margin-top: 28px;">
        <label class=" me-3" for="order_by_training"><input class="me-1" type="radio" checked name="order_by"
                                                            id="order_by_training" value="order_by_training">Upcoming
          Jobfairs</label>
        <label class=" me-3" for="order_by_training"><input class="me-1" type="radio" checked name="order_by"
                                                            id="order_by_training" value="order_by_training">Running
          Jobfairs</label>
        <label class=" me-3" for="order_by_training"><input class="me-1" type="radio" checked name="order_by"
                                                            id="order_by_training" value="order_by_training">Past
          Jobfairs</label>
      </div>
      <div class="text-end col-md-6">
        <div class="row">
          <div class="col-md-6">
            <label for="">From</label>
            <input type="date" class="form-control">
          </div>
          <div class="col-md-6">
            <label for="">To</label>
            <input type="date" class="form-control">
          </div>
        </div>

      </div>

      <div class="mt-5">
        <h2>Job Fair</h2>
        <hr>
      </div>


      <section class="container" style="max-width: 900px">
        <div class="row event-main-design-align mt-3">
          <div class="col-md-2 date-align">
            <img src="{{ asset('assets/admin/images/flags/french_flag.jpg') }}" alt="">
          </div>
          <div class="col-md-7 event-midle-align">
            <div class="member-info">
              {{--              <span class="work-shop-title">Name of Job Fair</span>--}}
              <h4 class="text-info">Name of Job Fair</h4>
              <p style="color: red;">Vanue</p>

              <div class="g-3">
                <div class="row">
                  <div class="col-md-6">
                    <div class="float-left time-align"><i class="bx bx-time icon-help"></i> <a>12-12-2022 10:00 AM</a></div>
                  </div>
                  <div class="col-md-6">
                    <div class="float-right time-align"><i class="bx bx-current-location icon-help"></i> <a>Organizer</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 text-center register-button-align">
            <a class="btn btn-info mt-4" href="{{ route('jobfair.details',1) }}">See details</a>

          </div>
        </div>
        <div class="row event-main-design-align mt-3">
          <div class="col-md-2 date-align">
            <img src="{{ asset('assets/admin/images/logo.png') }}" alt="">
          </div>
          <div class="col-md-7 event-midle-align">
            <div class="member-info">
              {{--              <span class="work-shop-title">Name of Job Fair</span>--}}
              <h4 class="text-info">Name of Job Fair</h4>
              <p style="color: red;">Vanue</p>

              <div class="g-3">
                <div class="row">
                  <div class="col-md-6">
                    <div class="float-left time-align"><i class="bx bx-time icon-help"></i> <a>12-12-2022 10:00 AM</a></div>
                  </div>
                  <div class="col-md-6">
                    <div class="float-right time-align"><i class="bx bx-current-location icon-help"></i> <a>Organizer</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 text-center register-button-align">

            <button class="btn btn-success mt-4" href="{{ route('jobfair.details',1) }}" style="background-color: #008080;">See details</button>
          </div>
        </div>

      </section>

    </div>

  </div>

@endsection
@section('script')

@endsection
