@extends('layout.site')

@section('stylesheet')
@endsection


@section('content')
  <div class="gallary">
    <p class="think-align-home">Lift</p>
    @foreach($core as $coremodule)
      <div class="hex-cell"><a class="hexa-design1" href="{{ $coremodule->link  }}"
                               style="background-image: url('{{ $coremodule->image }}'); transform: rotate(-30deg);background-repeat: no-repeat; background-position: center"><p class="text-align-design">{{$coremodule->name}}</p></a></div>
    @endforeach
    <p class="think-align-home2">Fit <span> Shift</span></p>
  </div>

@endsection
@section('script')

@endsection
