<!DOCTYPE html>
<html lang="en">

<head id="head">
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="Raihan Afroz; Saidul Islam; Dipta Day; Arif Hosen; Towhidul Islam;" name="author"/>
  <meta content="Touch & Solve" name="company"/>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <title>Institute Management System</title>

  <!-- Favicons -->
  <link href="{{asset('favicon.ico')}}" rel="icon">
  <link href="{{asset('favicon.ico')}}" rel="apple-touch-icon">
  {{--  <link href="{{asset('assets/frontend/img/favicon.png')}}" rel="icon">--}}
  {{--  <link href="{{asset('assets/frontend/img/apple-touch-icon.png')}}" rel="apple-touch-icon">--}}

  <!-- Google Fonts -->
  {{--    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">--}}

  <!-- Vendor CSS Files -->
  <link href="{{asset('assets/frontend/vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{asset('assets/frontend/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/frontend/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{asset('assets/frontend/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/frontend/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/frontend/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{asset('assets/frontend/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('assets/frontend/css/style.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('assets/frontend/css/custom.css')}}">

  @yield('stylesheet')

</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top ">
  <div class="container d-flex align-items-center">
{{--    <a href="{{ route('home') }}" class="logo"><img src="{{ asset('assets/text-logo.png') }}" alt="" class="img-fluid home-logo"></a>--}}

    <h1 class="logo me-auto home-text"><a href="{{route('home')}}">Institute Management System</a></h1>
    <!-- Uncomment below if you prefer to use an image logo -->
    <!-- <a href="index.html" class="logo me-auto"><img src="assets/frontend/img/logo.png" alt="" class="img-fluid"></a>-->

    <nav id="navbar" class="navbar">
      <ul>
        <li><a class="nav-link @if(request()->routeIs('home')) active @endif scrollto" href="{{route('home')}}">Home</a></li>

{{--        <li><a class="nav-link @if(request()->routeIs('jobfair.list')) active @endif scrollto" href="{{route('jobfair.list')}}">JPC-Placement</a></li>--}}
{{--        <li><a class="nav-link scrollto" href="{{route('linkpage')}}">Link Page</a></li>--}}
        <li><a class="nav-link @if(request()->routeIs('institute.trainings')) active @endif scrollto" href="{{route('institute.trainings')}}">Trainings</a></li>
        <li><a class="nav-link scrollto" href="#">Contact</a></li>
        <li>
          <a class="nav-link scrollto" href="#">Event</a>
        </li>
        {{--        <li><a class="nav-link scrollto" href="{{route('linkpage')}}">Link Page</a></li>--}}
        {{--        <li><a class="nav-link scrollto @if(request()->routeIs('institute.registration')) active @endif" href="{{route('institute.registration')}}">Institute Registration</a></li>--}}
        {{--                <li><a class="nav-link scrollto" href="#services">Services</a></li>--}}
        {{--                <li><a class="nav-link   scrollto" href="#portfolio">Portfolio</a></li>--}}
        {{--                <li><a class="nav-link scrollto" href="#team">Team</a></li>--}}
        {{--                <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>--}}
        {{--                    <ul>--}}
        {{--                        <li><a href="#">Drop Down 1</a></li>--}}
        {{--                        <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>--}}
        {{--                            <ul>--}}
        {{--                                <li><a href="#">Deep Drop Down 1</a></li>--}}
        {{--                                <li><a href="#">Deep Drop Down 2</a></li>--}}
        {{--                                <li><a href="#">Deep Drop Down 3</a></li>--}}
        {{--                                <li><a href="#">Deep Drop Down 4</a></li>--}}
        {{--                                <li><a href="#">Deep Drop Down 5</a></li>--}}
        {{--                            </ul>--}}
        {{--                        </li>--}}
        {{--                        <li><a href="#">Drop Down 2</a></li>--}}
        {{--                        <li><a href="#">Drop Down 3</a></li>--}}
        {{--                        <li><a href="#">Drop Down 4</a></li>--}}
        {{--                    </ul>--}}
        {{--                </li>--}}
        {{--                <li><a class="nav-link scrollto" href="{{route()}}">Register</a></li>--}}
        {{--        <li><a class="nav-link scrollto" href="#contact">Contact</a></li>--}}

        {{--        <li class="">--}}
        {{--          <div class="dropdown ">--}}
        {{--            <a class="custom-dropdown-button dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">--}}
        {{--              More--}}

        {{--            </a>--}}
        {{--            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">--}}
        {{--              <li><a class="dropdown-item" href="{{ route('institute.trainings.enrollTrainingList') }}">Enrolled Training</a></li>--}}

        {{--            </ul>--}}
        {{--          </div>--}}
        {{--        </li>--}}

        @guest()
          <li><a class="nav-link scrollto @if(request()->routeIs('register')) active @endif" href="{{route('register')}}">Registration</a></li>
          <li><a class="nav-link scrollto" href="{{route('login')}}">Login</a></li>
        @else
          @if(\App\Helper\CustomHelper::canView('', 'Institute Head'))
            <li><a class="nav-link" href="{{ route('form') }}">EAF</a></li>
          @endif
          <li><a class="nav-link scrollto" href="{{ route('admin.dashboard') }}">Dashboard</a></li>
          <li><a class="nav-link scrollto" href="{{ route('admin.logout') }}">Log Out</a></li>
        @endguest
      </ul>
      <i class="bi bi-list mobile-nav-toggle"></i>
    </nav><!-- .navbar -->

  </div>
</header><!-- End Header -->

<section style="min-height: 90vh" class="services section-bg">
  @yield('content')
</section>


<!-- ======= Footer ======= -->
<footer id="footer">

  {{--    <div class="footer-newsletter">--}}
  {{--        <div class="container">--}}
  {{--            <div class="row justify-content-center">--}}
  {{--                <div class="col-lg-6">--}}
  {{--                    <h4>Join Our Newsletter</h4>--}}
  {{--                    <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>--}}
  {{--                    <form action="" method="post">--}}
  {{--                        <input type="email" name="email"><input type="submit" value="Subscribe">--}}
  {{--                    </form>--}}
  {{--                </div>--}}
  {{--            </div>--}}
  {{--        </div>--}}
  {{--    </div>--}}

  {{--    <div class="footer-top">--}}
  {{--        <div class="container">--}}
  {{--            <div class="row">--}}

  {{--                <div class="col-lg-3 col-md-6 footer-contact">--}}
  {{--                    <h3>APRoMIS</h3>--}}
  {{--                    <p>--}}
  {{--                        H-14, 2rd Floor, Road No-08,  <br>--}}
  {{--                        Sector No-06<br>--}}
  {{--                        Uttora, Dhaka-1230. <br><br>--}}
  {{--                        <strong>Phone:</strong> +8801844177603<br>--}}
  {{--                        <strong>Email:</strong> info@touchandsolve.com<br>--}}
  {{--                    </p>--}}
  {{--                </div>--}}

  {{--                <div class="col-lg-3 col-md-6 footer-links">--}}
  {{--                    <h4>Useful Links</h4>--}}
  {{--                    <ul>--}}
  {{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>--}}
  {{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>--}}
  {{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>--}}
  {{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>--}}
  {{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>--}}
  {{--                    </ul>--}}
  {{--                </div>--}}

  {{--                <div class="col-lg-3 col-md-6 footer-links">--}}
  {{--                    <h4>Our Services</h4>--}}
  {{--                    <ul>--}}
  {{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>--}}
  {{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>--}}
  {{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>--}}
  {{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>--}}
  {{--                        <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>--}}
  {{--                    </ul>--}}
  {{--                </div>--}}

  {{--                <div class="col-lg-3 col-md-6 footer-links">--}}
  {{--                    <h4>Our Social Networks</h4>--}}
  {{--                    <p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p>--}}
  {{--                    <div class="social-links mt-3">--}}
  {{--                        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>--}}
  {{--                        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>--}}
  {{--                        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>--}}
  {{--                        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>--}}
  {{--                        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>--}}
  {{--                    </div>--}}
  {{--                </div>--}}

  {{--            </div>--}}
  {{--        </div>--}}
  {{--    </div>--}}

  <div class="container footer-bottom clearfix">
    <div class="copyright">
      &copy; Copyright <strong><span>Touch and Solve</span></strong>. All Rights Reserved
    </div>
    <div class="credits">
      Designed by <a href="#">Touch and Solve</a>
    </div>


  </div>
</footer><!-- End Footer -->

<div id="preloader"></div>
<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
{{--<script src="{{ asset('assets/admin/js/jquery.min.js') }}"></script>--}}
<script src="{{asset('assets/frontend/vendor/aos/aos.js')}}"></script>
<script src="{{asset('assets/frontend/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/frontend/vendor/glightbox/js/glightbox.min.js')}}"></script>
<script src="{{asset('assets/frontend/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('assets/frontend/vendor/php-email-form/validate.js')}}"></script>
<script src="{{asset('assets/frontend/vendor/swiper/swiper-bundle.min.js')}}"></script>
<script src="{{asset('assets/frontend/vendor/waypoints/noframework.waypoints.js')}}"></script>

<!-- Template Main JS File -->
<script src="{{asset('assets/frontend/js/main.js')}}"></script>

<script>
  function printDiv($id, $title = 'Document') {
    let head = document.getElementById("head").innerHTML;
    let divContents = document.getElementById($id).innerHTML;
    console.log(divContents)
    let a = window.open('', '', '');
    a.document.write('<!DOCTYPE html><html lang="en">');
    a.document.write(head);
    a.document.write(`<body onload="document.title = '${$title}'">`);
    a.document.write(divContents);
    a.document.write('</body></html>');
    a.document.close();
    setTimeout(function () {
      setTimeout(a.print(), 400)
      a.close();
    }, 500)
  }
</script>
@yield('script')

<script>
  $('select[name="division_id"]').change(function () {
    const $this = $('select[name="district_id"]')
    var idDivision = this.value;
    $this.html('');
    $.ajax({
      url: "{{url('api/fetch-districts')}}/" + idDivision,
      type: "GET",
      dataType: 'json',
      success: function (result) {
        $this.html('<option value="">Choose a district</option>');
        $.each(result.districts, function (key, value) {
          $this.append('<option value="' + value
            .id + '">' + value.name + '</option>');
        });
      }
    });
  });
  $('select[name="district_id"]').change(function () {
    const $this = $('select[name="upazila_id"]')
    var idUpazila = this.value;
    $this.html('');
    $.ajax({
      url: "{{url('api/fetch-upazilas')}}/" + idUpazila,
      type: "GET",
      dataType: 'json',
      success: function (result) {
        $this.html('<option value="">Choose a upazila</option>');
        $.each(result.upazilas, function (key, value) {
          $this.append('<option value="' + value
            .id + '">' + value.name + '</option>');
        });
      }
    });
  });
  $('select[name="institute_type_id"]').change(function () {
    const $this = $('select[name="institute_id"]')
    var idInstituteType = this.value;
    $this.html('');
    $.ajax({
      url: "{{url('api/fetch-institutes')}}/" + idInstituteType,
      type: "GET",
      dataType: 'json',
      success: function (result) {
        $this.html('<option value="">Choose a Institute</option>');
        $.each(result.institutes, function (key, value) {
          $this.append('<option value="' + value
            .id + '">' + value.name + '</option>');
        });
      }
    });
  });


  function checkWordValidation($id, $min, $max) {
    const $this = $('#' + $id)
    var $regex = /\s+/gi;
    var $wordcount = $.trim($this.val()).replace($regex, ' ').split(' ').length;
    if ($min > $wordcount) {
      $this.addClass('is-invalid')
      $('#' + $id + '_error').text('Minimum ' + $min + ' word required.')
    } else if ($wordcount > $max) {
      $this.addClass('is-invalid')
      $('#' + $id + '_error').text('Maximum ' + $max + ' word allow.')
    } else {
      $('#' + $id + '_error').text('')
      $this.removeClass('is-invalid')
    }
  }

</script>
</body>

</html>
