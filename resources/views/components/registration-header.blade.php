<div class="text-center">
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('pmu.registration')) btn-success text-white @endif" href="{{route('pmu.registration')}}">PMU</a>
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('institute.registration')) btn-success text-white @endif" href="{{route('institute.registration')}}">Institute</a>

    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('teacher.registration')) btn-success text-white @endif" href="{{route('teacher.registration')}}">Teacher</a>
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('register')) btn-success text-white @endif" href="{{ route('register') }}">Student</a>
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('trainee.registration')) btn-success text-white @endif" href="{{route('trainee.registration')}}">Trainee</a>
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('training.registration')) btn-success text-white @endif" href="{{route('training.registration')}}">Training Provider</a>
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('industry.registration')) btn-success text-white @endif" href="{{route('industry.registration')}}">Industry</a>
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('evalutor.registration')) btn-success text-white @endif" href="{{route('evalutor.registration')}}">Evaluator</a>
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('mentor.registration')) btn-success text-white @endif" href="{{route('mentor.registration')}}">Mentor</a>
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('isc.registration')) btn-success text-white @endif" href="{{route('isc.registration')}}">ISC</a>
    <a class="btn btn-outline-success m-2 tabClick" href="#">SPMU</a>
    <a class="btn btn-outline-success m-2 tabClick" href="#">TTC</a>
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('dte.registration')) btn-success text-white @endif" href="{{route('dte.registration')}}">DTE</a>
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('bmet.registration')) btn-success text-white @endif" href="{{route('bmet.registration')}}">BMET</a>
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('nsda.registration')) btn-success text-white @endif" href="{{route('nsda.registration')}}">NSDA</a>
    <a class="btn btn-outline-success m-2 tabClick" @if(request()->routeIs('tmed.registration')) btn-success text-white @endif" href="{{route('tmed.registration')}}">TMED</a>
    <a class="btn btn-outline-success m-2 tabClick" @if(request()->routeIs('bteb.registration')) btn-success text-white @endif" href="{{route('bteb.registration')}}">BTEB</a>
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('dgnm.registration')) btn-success text-white @endif" href="{{route('dgnm.registration')}}">DGNM</a>
    <a class="btn btn-outline-success m-2 tabClick" href="">DGME</a>
    <a class="btn btn-outline-success m-2 tabClick @if(request()->routeIs('association.registration')) btn-success text-white @endif" href="{{route('association.registration')}}">Association</a>
    <a class="btn btn-outline-success m-2 tabClick" @if(request()->routeIs('moi.registration')) btn-success text-white @endif" href="{{route('moi.registration')}}">MOI</a>
    <a class="btn btn-outline-success m-2 tabClick" href="#">TTTC</a>
    <a class="btn btn-outline-success m-2 tabClick text-uppercase" href="#">Nactar</a>
    <a class="btn btn-outline-success m-2 tabClick" href="#">VTTI</a>
    <a class="btn btn-outline-success m-2 tabClick" href="#">RPL</a>
    <a class="btn btn-outline-success m-2 tabClick" href="#">Agrani Bank</a>
    <a class="btn btn-outline-success m-2 tabClick" href="#">World Bank</a>
</div>

