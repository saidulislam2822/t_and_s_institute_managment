<?php

namespace App\Http\Controllers;

use App\Helper\CustomHelper;
use App\Helper\NotificationHelper;
use App\Helper\RedirectHelper;
use App\Models\BackgroundImage;
use App\Models\CoreModule;
use App\Models\District;
use App\Models\Division;
use App\Models\Institute;
use App\Models\Notification;
use App\Models\Training;
use App\Models\TrainingFile;
use App\Models\TrainingMember;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SiteController extends Controller {

  public function instituteTrainingList($id) {
    $today = (date('Y-m-d'));
    $today = date('Y-m-d', strtotime($today . ' +1 day'));
    $data['institute'] = Institute::find(request('id'));
    $data['datas'] = Training::where('institute_id', $id)->where('start_date', '>=', $today)->where('status', Training::$statusArrays[1])->orderBy('title', 'asc')->get();

    return view('site.institute-trainingsList', $data);
  }


// Training list
  public function indexTrainingList(Request $request) {
    $today = (date('Y-m-d'));
    $tomorrow = date('Y-m-d', strtotime($today . ' +1 day'));
    $data['institutes'] = Institute::whereHas('trainings', function ($q) use ($tomorrow) {
      $q->where('start_date', '>=', $tomorrow)->where('status', Training::$statusArrays[1]);
    })->withCount('trainings')->orderBy('name')->get();

    $data['trainings'] = Training::where('start_date', '>=', $tomorrow)->where('status', Training::$statusArrays[1])->orderBy('title', 'asc')->get();
    $data['upcomings'] = Training::where('start_date', '>=', $tomorrow)->where('status', Training::$statusArrays[1])->orderBy('title', 'asc')->get();
    $data['runnings'] = Training::where('start_date', '<=', $today)->where('end_date', '>=', $today)->where('status', Training::$statusArrays[1])->orderBy('title', 'asc')->get();
    $data['previouses'] = Training::where('end_date', '<', $today)->where('status', Training::$statusArrays[1])->orderBy('title', 'asc')->get();
    return view('site.trainingsList', $data);
  }

  // Training Member Form
  public function trainingDetails() {

    $data['training'] = Training::withCount('members')->with('institute.instituteHead', 'members')->where('id', \request('training_id'))->first();

    return view('site.training-details', $data);
  }

// training member store
  public function storeTrainingMember(Request $request, $trainingId) {
    $message = '<strong>Congratulations!!!</strong> Component successfully ';
    $rules = [
//            'user_id' => 'required|unique:'. with(new TrainingMember())->getTable() . 'user_id,training_id.'.$trainingId,
    ];
    $request->validate($rules);
    try {
      $trainingMember = new TrainingMember();
      $trainingMember->training_id = $trainingId;
      $trainingMember->user_id = auth()->id();
      $trainingMember->name = auth()->user()->name_en ?? auth()->user()->username;
      $trainingMember->email = auth()->user()->email;
      $trainingMember->phone = auth()->user()->phone;

      if ($trainingMember->save()) {
        $message = '<strong>Congratulations!!! Successfully Submitted.</strong>';
        NotificationHelper::create(Notification::$types[0], auth()->id(), 'Training Application.', 'Your application is submitted at '.now()->format('h:i A F d,Y'));
        return RedirectHelper::routeSuccessWithParams('institute.trainings.details', $message, [$trainingId]);
      }
      return RedirectHelper::backWithInput();

    } catch (Exception $e) {
      return RedirectHelper::backWithInputFromException();
    }

  }

  public function enrollTrainingList() {
    $data['trainings'] = TrainingMember::with('training.institute:name,id,photo')->where('user_id', auth()->id())->get();

    return view('site.enrollTrainingList', $data);
  }

  public function instituteRegistration() {
    $data['divisions'] = Division::get();
    $data['districts'] = District::get();
    return view('site.institute', $data);
  }


  public function cancelTrainingRequest($id) {
    try {
      $trainingMember = TrainingMember::find($id);
      if ($trainingMember->delete()) {
        return RedirectHelper::backWithInput();
      }
    } catch (\Exception $e) {
    }
  }


  public function home(){
      $data['core'] =CoreModule::where('status','=',CoreModule::$statusArrays[0])->get();
//      return $data;
      return view('site.index', $data);
  }
}
