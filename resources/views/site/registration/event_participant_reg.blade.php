@extends('layout.site')

@section('stylesheet')
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <style>
        .form-sub-section{
            background-color: rgba(25, 135, 84, 0.726) !important;
            /* color: black !important; */
        }
        .image_preview_container {
            width: 100px;
            height: 100px;
            background-color: lightgrey;
            margin-top: 12px;
        }
        .single-area {
            border: 1px dashed black;
            border-radius: 5px;
            padding: 10px;
            margin: 10px 0;
            background: #f4f4f4;
            /*transition: 1s;*/


  }




    </style>
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}">
<script src="{{ asset('assets/admin/plugins/sweetalert2/sweetalert2.all.js') }}"></script>
@endsection
@section('content')
  <div class="institute_body_align">

    <div class="container institute_head_alignment">
      <h2 class="text-center" id="title">Event Participant Form</h2>
      <hr>
      @if(session()->has('status'))
        {!! session()->get('status') !!}
      @endif
{{--      <x-registration-header />--}}
      <form class="form-horizontal toggleForm" action="" method="post" enctype="multipart/form-data">
      @csrf
        <div>
          <h4 class="p-2 text-light form-sub-section">Event Participant Information</h4>
        </div>
        <div  class="row mt-3">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Full name [English]<span class="text-danger">*</span></label>
                    <input type="text" name="name_en" placeholder="Full name in english" required value="{{ old('name_en') }}"
                           class="form-control @error('name_en') is-invalid @enderror">
                    @error('name_en')
                    <strong class="text-danger">{{ $errors->first('name_en') }}</strong>
                    @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">নাম [বাংলায়]<span class="text-danger">*</span></label>
                    <input type="text" name="name_bn" placeholder="বাংলায় পুরো নাম" required value="{{ old('name_bn') }}"
                           class="form-control @error('name_bn') is-invalid @enderror">
                    @error('name_bn')
                    <strong class="text-danger">{{ $errors->first('name_bn') }}</strong>
                    @enderror
                </div>
            </div>
        </div>
        <div  class="row mt-3">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Designation</label><br>
                    <select name="designation" id="designation" class="form-control @error('designation') is-invalid @enderror">
                        <option></option>
                        {{-- @foreach($institutes as $institute)
                          <option value="{{ $institute->id }}" @selected($institute->id == old('designation'))>{{ $institute->name }}</option>
                        @endforeach --}}
                        <option value="1">DG</option>
                        <option value="2">Director</option>
                        <option value="3">AD</option>
                        <option value="4">EO</option>
                        <option value="5">ATO</option>
                    </select>
                    @error('designation')
                    <strong class="text-danger">{{ $errors->first('designation') }}</strong>
                    @enderror
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group" id="dis">
                    <label for="district_id">District</label><br>
                    <select name="district_id" id="district_id" class="form-control @error('district_id') is-invalid @enderror">
                        <option value="">Choose Your District</option>
                        @foreach ($districts as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                    @error('district_id')
                    <strong class="text-danger">{{$error->first('district_id')}}</strong>
                    @enderror
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group" id="upa">
                    <label for="upazila_id">Upazila</label><br>
                    <select name="upazila_id" id="upazila_id" class="form-control @error('upazila_id') is-invalid @enderror">
                        <option></option>
                        @foreach ($upazilas as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                    @error('upazila_id')
                    <strong class="text-danger">{{$error->first('upazila_id')}}</strong>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row mt-3 mb-1" >
          <div class="col-md-4" id="td">
              <div class="form-group">
                  <label class="control-label">Distance From The Event Location (KM)</label>
                  <input id="distance" type="text" name="distance" placeholder="Enter Your Distance From The Event (KM)" required value="{{ old('distance') }}"
                         class="form-control select-or-disable @error('distance') is-invalid @enderror">
                  @error('distance')
                  <strong class="text-danger">{{ $errors->first('distance') }}</strong>
                  @enderror
              </div>
          </div>
          <div class="col-md-4" >
              <div class="form-group">
                  <label for="trade_technology">Trade/Technology</label><br>
                  <select name="trade_technology" id="trade_technology" class="select2 form-control @error('trade_technology') is-invalid @enderror">
                      <option></option>
                      @foreach ($technologies as $item)
                          <option value="{{$item->id}}">{{$item->name}}</option>
                      @endforeach
                  </select>
                  @error('trade_technology')
                  <strong class="text-danger">{{$error->first('trade_technology')}}</strong>
                  @enderror
              </div>
          </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Office<span class="text-danger">*</span></label>
                    <input type="text" name="office" placeholder="Enter Office Name" required value="{{ old('office') }}"
                           class="form-control @error('office') is-invalid @enderror">
                    @error('office')
                    <strong class="text-danger">{{ $errors->first('office') }}</strong>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row mt-3 mb-1">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="phone">Mobile Number <span class="text-danger">*</span></label>
                    <input type="number" name="phone" id="phone" placeholder="Enter Your Phone Number" autocomplete="off"
                           class="form-control @error('phone') is-invalid @enderror" value="{{ old('phone') }}">
                    <span class="spin"></span>
                    @error('phone')
                    <strong class="text-danger">{{ $errors->first('phone') }}</strong>
                    @enderror
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="alt_phone">Alternate Mobile Number <span class="text-danger">*</span></label>
                    <input type="number" name="alt_phone" id="alt_phone" placeholder="Enter Your Phone Number" autocomplete="off"
                           class="form-control @error('alt_phone') is-invalid @enderror" value="{{ old('alt_phone') }}">
                    <span class="spin"></span>
                    @error('alt_phone')
                    <strong class="text-danger">{{ $errors->first('alt_phone') }}</strong>
                    @enderror
                </div>
            </div>
{{--            <div class="row mb-3 mt-3">--}}
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">NID</label>
                    <input id="nid" type="text" name="nid" placeholder="Your NID NO." required value="{{ old('nid') }}"
                           class="form-control select-or-disable @error('nid') is-invalid @enderror">
                    @error('nid')
                    <strong class="text-danger">{{ $errors->first('nid') }}</strong>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row mt-3 mb-1">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="email">Email <span class="text-danger">*</span></label>
                        <input type="email" name="email" id="email" placeholder="Enter Your E-mail"
                               class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                        <span class="spin"></span>
                        @error('email')
                        <strong class="text-danger">{{ $errors->first('email') }}</strong>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Password<span class="text-danger">*</span></label>
                        <input type="password" name="password" placeholder="Password" required
                               value="{{ old('password') }}"
                               class="form-control @error('password') is-invalid @enderror">
                        <small id="realtime-password-error" class="text-danger d-none">Password must be at least one uppercase letter, one lowercase letter, one number and one special character </small>
                        @error('password')
                        <strong class="text-danger">{{ $errors->first('password') }}</strong>
                        @enderror
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Confirm Password<span class="text-danger">*</span></label>
                        <input type="password" name="password_confirmation" placeholder="Password" required
                               value="{{ old('password_confirmation') }}"
                               class="form-control @error('password_confirmation') is-invalid @enderror">
                        @error('password_confirmation')
                        <strong class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
                        @enderror
                        <small id="confirm-password-error" class="text-danger d-none">Enter the correct password</small>
                    </div>
                </div>
        </div>
        <div class="row mt-3 mb-1">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Username<span class="text-danger">*</span></label>
                        <input type="text" name="username" placeholder="Username" required value="{{ old('username') }}"
                               class="form-control @error('username') is-invalid @enderror">
                        @error('username')
                            <strong class="text-danger">{{ $errors->first('username') }}</strong>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="profile_photo_path">Image (Upload)</label>
                        <input type="file" name="profile_photo_path" id="profile_photo_path" placeholder="" autocomplete="off"
                               class="form-control @error('profile_photo_path') is-invalid @enderror" value="{{ old('profile_photo_path') }}">
                        <span class="spin"></span>
                        @error('profile_photo_path')
                        <strong class="text-danger">{{ $errors->first('profile_photo_path') }}</strong>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4 " >
                    <div class="image_preview_container text-center mx-auto" >
                        <p id="preview_text">Image Preview</p>
                        <img class="d-none mt-2"  id="image_preview" alt="your image" width="85" height="85" />
                    </div>
                </div>
        </div>
        <div class="mt-3">
          <h4 class="p-2 text-light form-sub-section">Payment Information</h4>
        </div>
          <div  class="row pb-3 mt-3 mt-3 mb-3" >
              <div class="col-md-4" id="acc">
                  <div class="form-group">
                      <label class="control-label">Account</label><br>
{{--                      <i class="fa fa-plus add-btn add-btn1 action-btn" title="add"></i>--}}
                      <select name="account_type" id="account_type" class="select2 form-control @error('account_type') is-invalid @enderror">
                          <option></option>
                          {{-- @foreach($institutes as $institute)
                            <option value="{{ $institute->id }}" @selected($institute->id == old('account_type'))>{{ $institute->name }}</option>
                          @endforeach --}}
                          <option value="1">BKash</option>
                          <option value="2">Nagad</option>
                      </select>
                      @error('account_type')
                      <strong class="text-danger">{{ $errors->first('account_type') }}</strong>
                      @enderror
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                      <label for="pay_phone">Account Adjacent Mobile Number <span class="text-danger">*</span></label>
                      <input type="number" name="pay_phone" id="pay_phone" placeholder="Enter Your Account Adjacent Phone Number" autocomplete="off"
                             class="form-control @error('pay_phone') is-invalid @enderror" value="{{ old('pay_phone') }}">
                      <span class="spin"></span>
                      @error('pay_phone')
                      <strong class="text-danger">{{ $errors->first('pay_phone') }}</strong>
                      @enderror
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">NID Used for Mobile Banking</label>
                      <input id="pay_nid" type="text" name="pay_nid" placeholder="Your NID NO." required value="{{ old('pay_nid') }}"
                             class="form-control select-or-disable @error('pay_nid') is-invalid @enderror">
                      @error('pay_nid')
                      <strong class="text-danger">{{ $errors->first('pay_nid') }}</strong>
                      @enderror
                  </div>
              </div>
          </div>
          <form class="form-horizontal toggleForm" action="" method="post" enctype="multipart/form-data">
              @csrf
              <div  class="row mt-3 d-none parent-area1" id="test">
              <div  class="row mt-3 single-area1" id="test1">
              <div  class="row mt-3 single-area1 single-area">

              <div>
                  <h5 class="p-2 text-light form-sub-section">Guest Information</h5>
              </div>

{{--                  <i class="fa fa-plus add-btn add-btn1 action-btn" title="add"></i>--}}
              <div  class="row mt-3">
                  <div class="col-md-6">
                      <div class="form-group">
                          <label class="control-label">Full name [English]<span class="text-danger">*</span></label>
                          <input type="text" name="name_en" placeholder="Full name in english" required value="{{ old('name_en') }}"
                                 class="form-control @error('name_en') is-invalid @enderror">
                          @error('name_en')
                          <strong class="text-danger">{{ $errors->first('name_en') }}</strong>
                          @enderror
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label class="control-label">নাম [বাংলায়]<span class="text-danger">*</span></label>
                          <input type="text" name="name_bn" placeholder="বাংলায় পুরো নাম" required value="{{ old('name_bn') }}"
                                 class="form-control @error('name_bn') is-invalid @enderror">
                          @error('name_bn')
                          <strong class="text-danger">{{ $errors->first('name_bn') }}</strong>
                          @enderror
                      </div>
                  </div>
              </div>
              <div  class="row mt-3">
                  <div class="col-md-4">
                      <div class="form-group">
                          <label class="control-label">Designation</label><br>
                          <select name="designation" id="designation2" class="select2 form-control @error('designation') is-invalid @enderror">
                              <option></option>
                              {{-- @foreach($institutes as $institute)
                                <option value="{{ $institute->id }}" @selected($institute->id == old('designation'))>{{ $institute->name }}</option>
                              @endforeach --}}
                              <option value="1">DG</option>
                              <option value="2">Director</option>
                              <option value="3">AD</option>
                              <option value="4">EO</option>
                              <option value="5">ATO</option>
                          </select>
                          @error('designation')
                          <strong class="text-danger">{{ $errors->first('designation') }}</strong>
                          @enderror
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group" id="dis2">
                          <label for="district_id">District</label><br>
                          <select name="district_id" id="district_id2" class="select2 form-control @error('district_id') is-invalid @enderror">
                              <option></option>
                              @foreach ($districts as $item)
                                  <option value="{{$item->id}}">{{$item->name}}</option>
                              @endforeach
                          </select>
                          @error('district_id')
                          <strong class="text-danger">{{$error->first('district_id')}}</strong>
                          @enderror
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group" id="upa2">
                          <label for="upazila_id">Upazila</label><br>
                          <select name="upazila_id" id="upazila_id2" class="select2 form-control @error('upazila_id') is-invalid @enderror">
                              <option></option>
                              @foreach ($upazilas as $item)
                                  <option value="{{$item->id}}">{{$item->name}}</option>
                              @endforeach
                          </select>
                          @error('upazila_id')
                          <strong class="text-danger">{{$error->first('upazila_id')}}</strong>
                          @enderror
                      </div>
                  </div>
              </div>
              <div class="row mt-3 mb-1">
                  <div class="col-md-4">
                      <div class="form-group">
                          <label class="control-label">Distance From The Event Location (KM)</label>
                          <input id="distance" type="text" name="distance" placeholder="Enter Your Distance From The Event (KM)" required value="{{ old('distance') }}"
                                 class="form-control select-or-disable @error('distance') is-invalid @enderror">
                          @error('distance')
                          <strong class="text-danger">{{ $errors->first('distance') }}</strong>
                          @enderror
                      </div>
                  </div>
                  <div class="col-md-4" id="td2">
                      <div class="form-group">
                          <label for="trade_technology">Trade/Technology</label><br>
                          <select name="trade_technology" id="trade_technology2" class="select2 trade_technology2 form-control @error('trade_technology') is-invalid @enderror">
                              <option></option>
                              <option value="1">Business</option>
                              <option value="2">Science</option>
                              @foreach ($technologies as $item)
                                  <option value="{{$item->id}}">{{$item->name}}</option>
                              @endforeach
                          </select>
                          @error('trade_technology')
                          <strong class="text-danger">{{$error->first('trade_technology')}}</strong>
                          @enderror
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group">
                          <label class="control-label">Office<span class="text-danger">*</span></label>
                          <input type="text" name="office" placeholder="Enter Office Name" required value="{{ old('office') }}"
                                 class="form-control @error('office') is-invalid @enderror">
                          @error('office')
                          <strong class="text-danger">{{ $errors->first('office') }}</strong>
                          @enderror
                      </div>
                  </div>
              </div>
              <div class="row mt-3 mb-1">
                  <div class="col-md-4">
                      <div class="form-group">
                          <label for="phone">Mobile Number <span class="text-danger">*</span></label>
                          <input type="number" name="phone" id="phone" placeholder="Enter Your Phone Number" autocomplete="off"
                                 class="form-control @error('phone') is-invalid @enderror" value="{{ old('phone') }}">
                          <span class="spin"></span>
                          @error('phone')
                          <strong class="text-danger">{{ $errors->first('phone') }}</strong>
                          @enderror
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group">
                          <label for="alt_phone">Alternate Mobile Number <span class="text-danger">*</span></label>
                          <input type="number" name="alt_phone" id="alt_phone" placeholder="Enter Your Phone Number" autocomplete="off"
                                 class="form-control @error('alt_phone') is-invalid @enderror" value="{{ old('alt_phone') }}">
                          <span class="spin"></span>
                          @error('alt_phone')
                          <strong class="text-danger">{{ $errors->first('alt_phone') }}</strong>
                          @enderror
                      </div>
                  </div>
                  {{--            <div class="row mb-3 mt-3">--}}
                  <div class="col-md-4">
                      <div class="form-group">
                          <label class="control-label">NID</label>
                          <input id="nid" type="text" name="nid" placeholder="Your NID NO." required value="{{ old('nid') }}"
                                 class="form-control select-or-disable @error('nid') is-invalid @enderror">
                          @error('nid')
                          <strong class="text-danger">{{ $errors->first('nid') }}</strong>
                          @enderror
                      </div>
                  </div>
              </div>
              <div class="row mt-3 mb-1">
                  <div class="col-md-4">
                      <div class="form-group">
                          <label for="email">Email <span class="text-danger">*</span></label>
                          <input type="email" name="email" id="email" placeholder="Enter Your E-mail"
                                 class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                          <span class="spin"></span>
                          @error('email')
                          <strong class="text-danger">{{ $errors->first('email') }}</strong>
                          @enderror
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group">
                          <label class="control-label">Password<span class="text-danger">*</span></label>
                          <input type="password" name="password" placeholder="Password" required
                                 value="{{ old('password') }}"
                                 class="form-control @error('password') is-invalid @enderror">
                          <small id="realtime-password-error" class="text-danger d-none">Password must be at least one uppercase letter, one lowercase letter, one number and one special character </small>
                          @error('password')
                          <strong class="text-danger">{{ $errors->first('password') }}</strong>
                          @enderror
                      </div>
                  </div>

                  <div class="col-md-4">
                      <div class="form-group">
                          <label class="control-label">Confirm Password<span class="text-danger">*</span></label>
                          <input type="password" name="password_confirmation" placeholder="Password" required
                                 value="{{ old('password_confirmation') }}"
                                 class="form-control @error('password_confirmation') is-invalid @enderror">
                          @error('password_confirmation')
                          <strong class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
                          @enderror
                          <small id="confirm-password-error" class="text-danger d-none">Enter the correct password</small>
                      </div>
                  </div>
              </div>
              <div class="row mt-3 mb-1">
                  <div class="col-md-4">
                      <div class="form-group">
                          <label class="control-label">Username<span class="text-danger">*</span></label>
                          <input type="text" name="username" placeholder="Username" required value="{{ old('username') }}"
                                 class="form-control @error('username') is-invalid @enderror">
                          @error('username')
                          <strong class="text-danger">{{ $errors->first('username') }}</strong>
                          @enderror
                      </div>
                  </div>
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label for="profile_photo_path">Image (Upload)</label>
                          <input type="file" name="profile_photo_path" id="profile_photo_path" placeholder="" autocomplete="off"
                                 class="form-control @error('profile_photo_path') is-invalid @enderror" value="{{ old('profile_photo_path') }}">
                          <span class="spin"></span>
                          @error('profile_photo_path')
                          <strong class="text-danger">{{ $errors->first('profile_photo_path') }}</strong>
                          @enderror
                      </div>
                  </div>
                  <div class="col-md-4 " >
                      <div class="image_preview_container text-center mx-auto" >
                          <p id="preview_text">Image Preview</p>
                          <img class="d-none mt-2"  id="image_preview" alt="your image" width="85" height="85" />
                      </div>
                  </div>
              </div>
                  <div class="row mt-3 mb-3">
                      <div class="text-right d-flex justify-content-center col-md-6">
                          <button class="btn btn-success add-btn1 text-center" onclick="add_guest()">Add Guest</button>
                      </div>
                      <div class="text-right d-flex justify-content-center col-md-6">
                          <button class="btn btn-success minus-btn1 text-center" onclick="remove_guest()">Remove Guest</button>
                      </div>
                  </div>
              </div>
              </div>
              </div>
          </form>


              <div style="text-align: center" class="row mt-3 mb-3" id="guest">
                  <div class="text-right d-flex justify-content-right">
                      <button class="btn btn-success text-center" onclick="changeStyle()">Add Guest</button>
                  </div>
              </div>
        <div style="text-align: center" class="row mt-3">
          <div class="text-right d-flex justify-content-center">
            <button class="btn btn-success text-center w-full" type="submit">Register</button>
          </div>
        </div>
        <div style="text-align: right" class="row mt-3">
          <div class="text-right justify-content-right">
            <a href="/login">Already Registered?</a>
          </div>
        </div>
      </form>

    </div>
  </div>
@endsection

@section('script')
{{-- nid or birth-certificate, year or semester one select from two   --}}
<script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
<script>
  $(document).ready(function(){
      $('#account_type').select2({
          placeholder: "Choose Account Type",
          dropdownParent: $('#acc')
      });
      $('#trade_technology').select2({
          placeholder: "Choose Your Trade/Technology",
          dropdownParent: $('#td')
      });
      $('.trade_technology2').select2({
          placeholder: "Choose Your Trade/Technology",
          dropdownParent: $('#td2')
      });
      $('#district_id').select2({
          placeholder: "Choose Your District",
          dropdownParent: $('#dis')
      });
      $('#upazila_id').select2({
          placeholder: "Choose Your Upazila",
          dropdownParent: $('#upa')
      });
      $('#district_id2').select2({
          placeholder: "Choose Your District",
          dropdownParent: $('#dis2')
      });
      $('#upazila_id2').select2({
          placeholder: "Choose Your Upazila",
          // dropdownParent: $('#upa2')
      });
      $('#designation').select2({
          placeholder: "Choose Your Designation",
          // dropdownParent: $('#upa2')
      });
      $('#designation2').select2({
          placeholder: "Choose Your Designation",
          // dropdownParent: $('#upa2')
      });
    $('.select-or-disable').change(function(e){
      const targetElementId = e.target.id;
      if(targetElementId == 'semester'){
        $('#year').attr('disabled','true');
        $(`#${targetElementId}`).removeAttr('disabled');
      }
      else if(targetElementId == 'year'){

        $('#semester').attr('disabled','true');
        $(`#${targetElementId}`).removeAttr('disabled');
      }
      else if(targetElementId == 'board_roll'){

        $('#running_board_roll').attr('disabled','disabled');
        $(`#${targetElementId}`).removeAttr('disabled');
      }
      else if(targetElementId == 'running_board_roll'){

        $('#board_roll').attr('disabled','disabled');
        $(`#${targetElementId}`).removeAttr('disabled');
      }
      else if(targetElementId == 'birth_certificate'){

        $('#nid').attr('disabled','disabled');
        $(`#${targetElementId}`).removeAttr('disabled');
      }
      else {
        $('#birth_certificate').attr('disabled','disabled');
        $(`#${targetElementId}`).removeAttr('disabled');
      }
      console.log('select or disable', e.target.id)

    })
    $(document).on('click', '.minus-btn1', function () {
        $(this).closest('.single-area1').remove()
    })
    $(document).on('click', '.add-btn1', function () {
        const content = $('#test1').html()
        $('.parent-area1').append(content)
        // $('#trade_technology2').lastChild.addClass('tdt2');
        $('#trade_technology2').select2();
        // $('.trade_technology2').last().remove();

    })
  })
  function changeStyle(){
      $('#test').removeClass('d-none');
      $('#guest').addClass('d-none');
  }

</script>
<script>
  $(document).ready(function(){
    $(document).on('keyup','input[name="password"]',function(e){
      let password = e.target.value;
      // Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
      const regExp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
      // console.log('passRegex ', password)

      if(password.match(regExp)){
        $('#realtime-password-error').addClass('d-none');
      }
      else{
        $('#realtime-password-error').removeClass('d-none');
      }

    })

    // confirm password event handler
    $('input[name="password_confirmation"]').keyup(function(){
      const confirmPassword =  $('input[name="password_confirmation"]').val();
      const password =  $('input[name="password"]').val();
      let confirmPasswordError =  $('#confirm-password-error');

      if(confirmPassword != password){
        confirmPasswordError.removeClass('d-none')
      }
      else{
        confirmPasswordError.addClass('d-none')
      }
    });

    // for code
    $(document).on('keyup','input[name="code"]',function(e){
     let codeValue = $('input[name = "code"]').val();
     if(codeValue.length < 7){
      $('#realtime-code-error').removeClass('d-none');
     }
     else{
      $('#realtime-code-error').addClass('d-none')
     }
    })

    // fucntion call for word count- from site blade
    $('#summernote').keyup(function(){
      checkWordValidation('summernote', 200, 500)
    })
  })
  </script>


    {{-- date picker --}}
    <script src="{{ asset('assets/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script>
    $(document).ready(function () {
      $(".yearPicker").datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        startDate: '-50y',
        endDate: '+30y',
        autoclose: true
      });
    });
  </script>
  <script>
 $(document).ready(function(){
    $('#profile_photo_path').change(function(e){
        {
            $('#image_preview').attr('src', window.URL.createObjectURL(e.target.files[0]));
            $('#image_preview').removeClass('d-none');
            $('#preview_text').addClass('d-none');
        console.log("image selected", e.target.files.name);
    }
    })
 })

  </script>
@endsection

