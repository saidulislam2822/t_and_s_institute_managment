@extends('layout.admin')

@section('stylesheet')
  <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/admin/plugins/summernote/summernote-bs4.css') }}">
  <style>
      /*.img-input{*/
      /*    text-align: center;*/
      /*}*/
      #inp {
          text-align: center;
          margin: auto;
      }
     .sub-panel-heading .sub-panel-title {
          font-size: 20px;
          font-weight: 400;
          margin-bottom: 26px;
          text-align: left;
      }

      .select2-selection__rendered {
          line-height: 31px !important;
      }
      .select2-container .select2-selection--single {
          height: 40px !important;
      }
      .select2-selection__arrow {
          height: 34px !important;
      }
  </style>
@endsection

@section('content')
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <section class="panel">
            <header class="panel-heading">
              <h2 class="panel-title">Update Resume</h2>
            </header>
            <div class="panel-body">
              @if(\App\Helper\CustomHelper::canView('List Of Student', 'Super Admin'))
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-xl-12 text-right mb-3">
                    {{--                    <a href="{{ route('admin.division.list') }}" class="brn btn-success btn-sm">List of divisions</a>--}}
                  </div>
                </div>
              @endif
              @if(session()->has('status'))
                {!! session()->get('status') !!}
              @endif
                <div class="row">
                  <div class="col-sm-6">
                    <div class="img-holder mb-4 d-flex justify-content-center">
                      <img src="{{ asset('assets/placeholder.png') }}"
                           alt="example placeholder" style="width: 131px; margin-top: 26px;" />
                    </div>
                    <div class="form-group" style="text-align-last: center;">
                      <label for="image">Photo (Upload)</label>
                      <input id="inp" type="file" name="image" id="image" placeholder="Enter Your Name" autocomplete="off"
                             class="form-control @error('image') is-invalid @enderror" value="{{ old('image') }}">
                      <span class="spin"></span>
                      @error('image')
                      <strong class="text-danger">{{ $errors->first('image') }}</strong>
                      @enderror
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="control-label">Full name [English]<span class="text-danger">*</span></label>
                          <input type="text" name="name_en" placeholder="Full name in english" required value="{{ old('name_en') }}"
                                 class="form-control @error('name_en') is-invalid @enderror">
                          @error('name_en')
                          <strong class="text-danger">{{ $errors->first('name_en') }}</strong>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="phone">Phone <span class="text-danger">*</span></label>
                          <input type="number" name="phone" id="phone" placeholder="Enter Your Phone Number" autocomplete="off"
                                 class="form-control @error('phone') is-invalid @enderror" value="{{ old('phone') }}">
                          <span class="spin"></span>
                          @error('phone')
                          <strong class="text-danger">{{ $errors->first('phone') }}</strong>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="email">Email <span class="text-danger">*</span></label>
                          <input type="email" name="email" id="email" placeholder="Enter Your E-mail"
                                 class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                          <span class="spin"></span>
                          @error('email')
                          <strong class="text-danger">{{ $errors->first('email') }}</strong>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="control-label" for="address">Address</label>
                          <input type="text" name="address" placeholder="address" autocomplete="off"
                                 value="{{ old('address') }}"
                                 class="form-control @error('address') is-invalid @enderror" disabled>
                          @error('address')
                          <strong class="text-danger">{{ $errors->first('address') }}</strong>
                          @enderror
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <header class="sub-panel-heading" style="background-color: #e9eff1">
                  <h2 class="sub-panel-title">Registration Information</h2>
                </header>
{{--                <div class="row">--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <label class="control-label">University/Polytechnic/TSC/IHT/IMT/MATS/NI</label>--}}
{{--                      <select name="job_location" class="select2 form-control @error('job_location') is-invalid @enderror" style="width: 10px !important;">--}}
{{--                        <option value="">Choose an option</option>--}}
{{--                        <option value="">University 1</option>--}}
{{--                        <option value="">University 2</option>--}}
{{--                      </select>--}}
{{--                      @error('job_location')--}}
{{--                      <strong class="text-danger">{{ $errors->first('job_location') }}</strong>--}}
{{--                      @enderror--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <div class="well well-sm text-center">--}}
{{--                        <div class="row">--}}
{{--                          <label class="control-label">Student Status</label>--}}
{{--                        </div>--}}
{{--                        <div class="btn-group" data-toggle="buttons">--}}

{{--                          <label class="btn btn-success active">--}}
{{--                            <input type="radio" name="options" id="option2" autocomplete="off" chacked>--}}
{{--                            <span class="glyphicon glyphicon-ok"> Ex-Student</span>--}}
{{--                          </label>--}}

{{--                          <label class="btn btn-primary">--}}
{{--                            <input type="radio" name="options" id="option1" autocomplete="off">--}}
{{--                            <span class="glyphicon glyphicon-ok">Current-Student</span>--}}
{{--                          </label>--}}
{{--                        </div>--}}
{{--                      </div>--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <label class="control-label">Department/Technology/Trade </label>--}}
{{--                      <select name="technology" class="select2 form-control @error('technology') is-invalid @enderror" style="width: 10px !important;">--}}
{{--                        <option value="">Choose an option</option>--}}
{{--                        <option value="">Trade 1</option>--}}
{{--                        <option value="">Trade 2</option>--}}
{{--                      </select>--}}
{{--                      @error('technology')--}}
{{--                      <strong class="text-danger">{{ $errors->first('technology') }}</strong>--}}
{{--                      @enderror--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <label class="control-label">Shift(Optional)</label>--}}
{{--                      <select name="shift" class="select2 form-control @error('shift') is-invalid @enderror" style="width: 10px !important;">--}}
{{--                        <option value="">Choose an option</option>--}}
{{--                        <option value="">Shift 1</option>--}}
{{--                        <option value="">Shift 2</option>--}}
{{--                      </select>--}}
{{--                      @error('shift')--}}
{{--                      <strong class="text-danger">{{ $errors->first('shift') }}</strong>--}}
{{--                      @enderror--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <label class="control-label" for="section">Section(Optional)</label>--}}
{{--                      <input type="text" name="section" placeholder="Saction" autocomplete="off"--}}
{{--                             value="{{ old('section') }}"--}}
{{--                             class="form-control @error('section') is-invalid @enderror">--}}
{{--                      @error('section')--}}
{{--                      <strong class="text-danger">{{ $errors->first('section') }}</strong>--}}
{{--                      @enderror--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <label class="control-label" for="year">Year</label>--}}
{{--                      <input type="text" name="year" placeholder="Year" autocomplete="off"--}}
{{--                             value="{{ old('year') }}"--}}
{{--                             class="form-control @error('year') is-invalid @enderror">--}}
{{--                      @error('year')--}}
{{--                      <strong class="text-danger">{{ $errors->first('year') }}</strong>--}}
{{--                      @enderror--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <div class="well well-sm ">--}}
{{--                        <div class="row">--}}
{{--                          <div class="col-md-5">--}}
{{--                            <div class="btn-group" data-toggle="buttons">--}}
{{--                              <label class="btn btn-info active">--}}
{{--                                <input type="radio" name="options" id="option2" autocomplete="off" chacked>--}}
{{--                                <span class="glyphicon glyphicon-ok"> Year</span>--}}
{{--                              </label>--}}
{{--                              <label class="btn btn-danger">--}}
{{--                                <input type="radio" name="options" id="option1" autocomplete="off">--}}
{{--                                <span class="glyphicon glyphicon-ok">Semester</span>--}}
{{--                              </label>--}}
{{--                            </div>--}}
{{--                          </div>--}}
{{--                          <div class="col-md-7">--}}
{{--                            <input type="text" name="year" placeholder="Year/Semester" autocomplete="off"--}}
{{--                                   value="{{ old('year') }}"--}}
{{--                                   class="form-control @error('year') is-invalid @enderror">--}}
{{--                          </div>--}}
{{--                        </div>--}}
{{--                      </div>--}}
{{--                    </div>--}}

{{--                    --}}{{--                      <div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <div class="well well-sm ">--}}
{{--                        <div class="row">--}}
{{--                          <div class="col-md-7">--}}
{{--                            <div class="btn-group" data-toggle="buttons">--}}
{{--                              <label class="btn btn-info active">--}}
{{--                                <input type="radio" name="options" id="option2" autocomplete="off" chacked>--}}
{{--                                <span class="glyphicon glyphicon-ok">SSC Board Roll</span>--}}
{{--                              </label>--}}
{{--                              <label class="btn btn-danger">--}}
{{--                                <input type="radio" name="options" id="option1" autocomplete="off">--}}
{{--                                <span class="glyphicon glyphicon-ok">Running Board Roll</span>--}}
{{--                              </label>--}}
{{--                            </div>--}}
{{--                          </div>--}}
{{--                          <div class="col-md-5">--}}
{{--                            <input type="text" name="year" placeholder="Board Roll" autocomplete="off"--}}
{{--                                   value="{{ old('year') }}"--}}
{{--                                   class="form-control @error('year') is-invalid @enderror">--}}
{{--                          </div>--}}
{{--                        </div>--}}
{{--                      </div>--}}
{{--                    </div>--}}

{{--                    --}}{{--                      <div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <label class="control-label" for="saction">Addmission Year</label>--}}
{{--                      <input type="text" name="admission_year" placeholder="Addmission Year" autocomplete="off"--}}
{{--                             value="{{ old('admission_year') }}"--}}
{{--                             class="form-control @error('admission_year') is-invalid @enderror">--}}
{{--                      @error('admission_year')--}}
{{--                      <strong class="text-danger">{{ $errors->first('admission_year') }}</strong>--}}
{{--                      @enderror--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <label class="control-label" for="session">Session</label>--}}
{{--                      <input type="text" name="session" placeholder="xxxx-xx (2021-22)" autocomplete="off"--}}
{{--                             value="{{ old('session') }}"--}}
{{--                             class="form-control @error('session') is-invalid @enderror">--}}
{{--                      @error('session')--}}
{{--                      <strong class="text-danger">{{ $errors->first('session') }}</strong>--}}
{{--                      @enderror--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <label class="control-label" for="nid">NID</label>--}}
{{--                      <input type="text" name="nid" placeholder="NID" autocomplete="off"--}}
{{--                             value="{{ old('nid') }}"--}}
{{--                             class="form-control @error('nid') is-invalid @enderror">--}}
{{--                      @error('nid')--}}
{{--                      <strong class="text-danger">{{ $errors->first('nid') }}</strong>--}}
{{--                      @enderror--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <label class="control-label" for="ber">BER</label>--}}
{{--                      <input type="text" name="ber" placeholder="BER" autocomplete="off"--}}
{{--                             value="{{ old('ber') }}"--}}
{{--                             class="form-control @error('ber') is-invalid @enderror">--}}
{{--                      @error('ber')--}}
{{--                      <strong class="text-danger">{{ $errors->first('ber') }}</strong>--}}
{{--                      @enderror--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                </div>--}}
                  <div  class="row pb-3 mt-3 border-bottom">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="trade_technology">Trade/Technology</label>
                        <select name="trade_technology" id="trade_technology" class="form-control @error('trade_technology') is-invalid @enderror">
                          <option value="">Choose a Trade/Technology</option>
{{--                          @foreach ($departments as $item)--}}
{{--                            <option value="{{$item->id}}">{{$item->name}}</option>--}}
{{--                          @endforeach--}}
                        </select>
                        @error('trade_technology')
                        <strong class="text-danger">{{$error->first('trade_technology')}}</strong>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="shift">Shift</label>
                        <select name="shift" id="shift" class="form-control @error('shift') is-invalid @enderror">
                          <option value="">Choose a Shift</option>
{{--                          @foreach ($departments as $item)--}}
{{--                            <option value="{{$item->id}}">{{$item->name}}</option>--}}
{{--                          @endforeach--}}
                        </select>
                        @error('shift')
                        <strong class="text-danger">{{$error->first('shift')}}</strong>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="section">Section (optional) </label>
                        <select name="section" id="section" class="form-control @error('section') is-invalid @enderror">
                          <option value="">Choose a Section</option>
{{--                          @foreach ($departments as $item)--}}
{{--                            <option value="{{$item->id}}">{{$item->name}}</option>--}}
{{--                          @endforeach--}}
                        </select>
                        @error('section')
                        <strong class="text-danger">{{$error->first('section')}}</strong>
                        @enderror
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label class="control-label">Department</label>
                        <select name="department" class="form-control @error('department') is-invalid @enderror">
                          <option value="">Choose a Department</option>
                          <option value="1">Computer Science and Engineering</option>
                          <option value="2">Physics</option>
                          <option value="3">Chemical Engineering</option>
                          {{-- @foreach($divisions as $division)
                            <option value="{{ $division->id }}" @selected($division->id == old('department'))>{{ $division->name }}</option>
                          @endforeach --}}
                        </select>
                        @error('department')
                        <strong class="text-danger">{{ $errors->first('department') }}</strong>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="control-label">Semester</label>
                        <select id="semester" name="semester" class="form-control select-or-disable @error('semester') is-invalid @enderror">
                          <option value="">Choose a Semester</option>
                          <option value="1">Semester 1</option>
{{--                          @foreach($semesters as $item)--}}
{{--                            <option value="{{ $item->id }}" @selected($item->id == old('semester'))>{{ $item->name }}</option>--}}
{{--                          @endforeach--}}
                        </select>
                        @error('semester')
                        <strong class="text-danger">{{ $errors->first('semester') }}</strong>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="control-label">Year</label>
                        <select id="year"  name="year" class="form-control select-or-disable @error('year') is-invalid @enderror">
                          <option value="">Choose a Year</option>
                          <option value="1">Year 1</option>
                          <option value="2">Year 2</option>
                          <option value="3">Year 3</option>
                          <option value="4">Year 4</option>
                          {{-- @foreach($divisions as $division)
                            <option value="{{ $division->id }}" @selected($division->id == old('year'))>{{ $division->name }}</option>
                          @endforeach --}}
                        </select>
                        @error('year')
                        <strong class="text-danger">{{ $errors->first('year') }}</strong>
                        @enderror
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="session">Session<span class="text-danger">*</span></label>
                        <input type="text" name="session" id="session" placeholder="2021-2022" autocomplete="off"
                               class=" form-control @error('session') is-invalid @enderror" value="{{ old('session') }}">
                        <span class="spin"></span>
                        @error('session')
                        <strong class="text-danger">{{ $errors->first('session') }}</strong>
                        @enderror
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="board_roll">Board Roll<span class="text-danger">*</span></label>
                        <input type="text" name="board_roll" id="board_roll" placeholder="Enter Your Fath er Name" autocomplete="off"
                               class="form-control select-or-disable @error('board_roll') is-invalid @enderror" value="{{ old('board_roll') }}">
                        <span class="spin"></span>
                        @error('board_roll')
                        <strong class="text-danger">{{ $errors->first('board_roll') }}</strong>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="running_board_roll">Running Board Roll<span class="text-danger">*</span></label>
                        <input type="text" name="running_board_roll" id="running_board_roll" placeholder="Enter Your Fath er Name" autocomplete="off"
                               class="form-control select-or-disable @error('running_board_roll') is-invalid @enderror" value="{{ old('running_board_roll') }}">
                        <span class="spin"></span>
                        @error('running_board_roll')
                        <strong class="text-danger">{{ $errors->first('running_board_roll') }}</strong>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="admission_year">Admission Year <span class="text-danger">*</span></label>
                        <input type="text" name="admission_year" id="admission_year" placeholder="Enter Your Fath er Name" autocomplete="off"
                               class="form-control @error('admission_year') is-invalid @enderror" value="{{ old('admission_year') }}">
                        <span class="spin"></span>
                        @error('admission_year')
                        <strong class="text-danger">{{ $errors->first('admission_year') }}</strong>
                        @enderror
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="control-label">NID<span class="text-danger">*</span></label>
                        <input id="nid" type="text" name="nid" placeholder="Your NID NO." required value="{{ old('nid') }}"
                               class="form-control select-or-disable @error('nid') is-invalid @enderror">
                        @error('nid')
                        <strong class="text-danger">{{ $errors->first('nid') }}</strong>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="control-label">Birth-Certificate<span class="text-danger">*</span></label>
                        <input type="text" id="birth_certificate" name="birth_certificate" placeholder="Your Birth Certificate NO." required value="{{ old('birth_certificate') }}"
                               class="form-control select-or-disable @error('birth_certificate') is-invalid @enderror">
                        @error('birth_certificate')
                        <strong class="text-danger">{{ $errors->first('birth_certificate') }}</strong>
                        @enderror
                      </div>
                    </div>
                  </div>
                <div class="row mb-4">
                  <div class="col-sm-12 text-right">
                   <button class="btn btn-danger btn-sm" type="submit">Update</button>
                  </div>
                </div>
                <header class="sub-panel-heading" style="background-color: #e9eff1">
                  <h2 class="sub-panel-title">Training/Short Course Information</h2>
                </header>
                <div class="row" style="width: 100%;">
                  <div class="col-sm-12" style="text-align: left; margin-bottom: 30px; padding-left: 10px;">
                  <p>
                    <a class="btn btn-success btn-md" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" style="width: 100%;">
                      <i class="mdi mdi-plus-circle-outline"></i> ADD
                    </a>
                  </p>
                  <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                      <div class="row">

                        <div class="col-sm-12">
                          <div class="form-group">
                            <label class="control-label">Training Provider<span class="text-danger">*</span></label>
                            <select name="provider_id" class="form-control @error('provider_id') is-invalid @enderror">
                              <option value=""></option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label class="control-label">Training Title<span class="text-danger">*</span></label>
                            <input type="text" name="title" placeholder="Training Title" autocomplete="off" required
                                   value="{{ old('title') }}"
                                   class="form-control @error('title') is-invalid @enderror">
                            @error('title')
                            <strong class="text-danger">{{ $errors->first('title') }}</strong>
                            @enderror
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label class="control-label">Start Date<span class="text-danger">*</span></label>
                            <input type="date" name="start_date" placeholder="Title" autocomplete="off" required
                                   value="{{ old('start_date') }}"
                                   class="form-control @error('start_date') is-invalid @enderror">
                            @error('start_date')
                            <strong class="text-danger">{{ $errors->first('start_date') }}</strong>
                            @enderror
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label class="control-label">End Date<span class="text-danger">*</span></label>
                            <input type="date" name="end_date" placeholder="Title" autocomplete="off" required
                                   value="{{ old('end_date') }}"
                                   class="form-control @error('end_date') is-invalid @enderror">
                            @error('end_date')
                            <strong class="text-danger">{{ $errors->first('end_date') }}</strong>
                            @enderror
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label class="control-label">Training Location<span class="text-danger">*</span></label>
                            <input type="text" name="training_location" placeholder="Training Location" autocomplete="off" required
                                   value="{{ old('training_location') }}"
                                   class="form-control @error('training_location') is-invalid @enderror">
                            @error('training_location')
                            <strong class="text-danger">{{ $errors->first('training_location') }}</strong>
                            @enderror
                          </div>
                        </div>

                        <div class="col-sm-4">
                          <div class="form-group">
                            <label class="control-label">Country<span class="text-danger">*</span></label>
                            <select name="country_id" class="form-control @error('country_id') is-invalid @enderror" required>
                              <option value="">Choose a country</option>
                            </select>
                            @error('country_id')
                            <strong class="text-danger">{{ $errors->first('country_id') }}</strong>
                            @enderror
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label class="control-label">Duration<span class="text-danger">*</span></label>
                            <div class="row">
                              <div class="col-md-4">
                                <input type="text" name="duration_year" placeholder="Year" autocomplete="off" required
                                       value="{{ old('duration_year') }}"
                                       class="form-control @error('duration_year') is-invalid @enderror"></div>
                              <div class="col-md-4">
                                <input type="text" name="duration" placeholder="Month" autocomplete="off" required
                                       value="{{ old('duration_month') }}"
                                       class="form-control @error('duration_month') is-invalid @enderror"></div>
                              <div class="col-md-4">
                                <input type="text" name="duration" placeholder="Day" autocomplete="off" required
                                       value="{{ old('duration_day') }}"
                                       class="form-control @error('duration_day') is-invalid @enderror"></div>
                            </div>
                            @error('duration')
                            <strong class="text-danger">{{ $errors->first('duration') }}</strong>
                            @enderror
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12 text-right">
                          <button class="btn btn-danger btn-sm" type="submit">Submit</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                </div>
                <div class="row">
                  <table id="datatable-buttons" class="table table-striped dt-responsive nowrap"
                         cellspacing="0" width="100%" style="font-size: 14px; margin: 0px 0px 10px 10px;">
                    <thead>
                    <tr>
                      <th width="50">#</th>
                      <th>Training title</th>
                      <th>Provider</th>
                      <th>Duration</th>
                      <th>Location</th>
                      <th>Option</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    {{--                @foreach($datas as $key => $val)--}}
                    {{--                  <tr class="@if(($key%2) == 0)gradeX @else gradeC @endif">--}}
                    {{--                    <td class="p-1">{{ ($key+1) }}</td>--}}
                    {{--                    <td class="p-1 text-capitalize"><a href="{{ route('institute.trainings.details', $val->training_id) }}">{{ $val->training?->title }}</a></td>--}}
                    {{--                    <td class="p-1 text-capitalize">{{ $val->training?->institute?->name }}</td>--}}
                    {{--                    <td class="p-1">{{ $val->training?->start_date . ' to ' . $val->training?->end_date }}</td>--}}
                    {{--                    <td class="p-1">{{ $val->created_at->format('h:i A F d, Y') }}</td>--}}
                    {{--                    <td class="p-1 text-capitalize">{{ $val->status }}</td>--}}
                    {{--                    <td class="p-0">--}}
                    {{--                      @if($val->created_at == $val->updated_at && $val->status == \App\Models\TrainingMember::$statusArrays[0] )--}}
                    {{--                        <a href="{{ route('admin.my.training.withdraw', $val->id) }}" class="btn btn-sm btn-warning" style="cursor: pointer">Withdraw</a>--}}
                    {{--                      @endif--}}
                    {{--                    </td>--}}
                    {{--                  </tr>--}}
                    {{--                @endforeach--}}
                    </tbody>
                  </table>
                </div>

                <header class="sub-panel-heading" style="background-color: #e9eff1">
                  <h2 class="sub-panel-title">Educational Information</h2>
                </header>
                <div class="row" style="width: 100%;">
                  <div class="col-sm-12" style="text-align: center; margin-bottom: 30px;">
                    <p>
                      <a class="btn btn-success btn-md" data-toggle="collapse" href="#education_info" role="button" aria-expanded="false" aria-controls="education_info" style="width: 100%;">
                        <i class="mdi mdi-plus-circle-outline"></i> ADD
                      </a>
                    </p>
                    <div class="collapse" id="education_info">
                      <div class="card card-body">
                        <div class="row">

                          <div class="col-sm-12">
                            <div class="form-group">
                              <label class="control-label">Degree<span class="text-danger">*</span></label>
                              <select name="degree" class="form-control @error('degree') is-invalid @enderror">
                                <option value="">BA (Bachelor of Arts)</option>
                                <option value="">BSc (Bachelor of Science)</option>
                                <option value="">BBS (Bachelor of Business Studies)</option>
                                <option value="">SSC (Secondary School Certificate)</option>
                                <option value="">HSC (Higher Secondary School Certificate)</option>
                              </select>
                              @error('degree')
                              <strong class="text-danger">{{ $errors->first('degree') }}</strong>
                              @enderror
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label class="control-label">Institution<span class="text-danger">*</span></label>
                              <input type="text" name="institution" placeholder="Institution" autocomplete="off" required
                                     value="{{ old('institution') }}"
                                     class="form-control @error('institution') is-invalid @enderror">
                              @error('institution')
                              <strong class="text-danger">{{ $errors->first('institution') }}</strong>
                              @enderror
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label class="control-label">Subject/Department<span class="text-danger">*</span></label>
                              <input type="text" name="subject" placeholder="Subject/Department" autocomplete="off" required
                                     value="{{ old('subject') }}"
                                     class="form-control @error('subject') is-invalid @enderror">
                              @error('subject')
                              <strong class="text-danger">{{ $errors->first('subject') }}</strong>
                              @enderror
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label class="control-label">Year<span class="text-danger">*</span></label>
                              <input type="date" name="year" placeholder="Year" autocomplete="off" required
                                     value="{{ old('year') }}"
                                     class="form-control @error('year') is-invalid @enderror">
                              @error('year')
                              <strong class="text-danger">{{ $errors->first('year') }}</strong>
                              @enderror
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label class="control-label">Board<span class="text-danger">*</span></label>
                              <select name="board" class="form-control @error('board') is-invalid @enderror">
                                <option value="">Dhaka</option>
                                <option value="">Khulna</option>
                              </select>
                              @error('board')
                              <strong class="text-danger">{{ $errors->first('board') }}</strong>
                              @enderror
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label class="control-label">Result<span class="text-danger">*</span></label>
                              <input type="text" name="result" placeholder="Result" autocomplete="off" required
                                     value="{{ old('result') }}"
                                     class="form-control @error('result') is-invalid @enderror">
                              @error('result')
                              <strong class="text-danger">{{ $errors->first('result') }}</strong>
                              @enderror
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label class="control-label">GPA Scale<span class="text-danger">*</span></label>
                              <select name="gpa_scale" class="form-control @error('gpa_scale') is-invalid @enderror">
                                <option value="">GPA 5</option>
                                <option value="">CGPA 4</option>
                              </select>
                              @error('gpa_scale')
                              <strong class="text-danger">{{ $errors->first('gpa_scale') }}</strong>
                              @enderror
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12 text-right">
                            <button class="btn btn-danger btn-sm" type="submit">Submit</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12">

                    <div class="row">
                      <table id="datatable-buttons" class="table table-striped dt-responsive nowrap"
                             cellspacing="0" width="100%" style="font-size: 14px; margin: 0px 0px 10px 10px;">
                        <thead>
                        <tr>
                          <th width="50">#</th>
                          <th>Degree</th>
                          <th>Institution</th>
                          <th>Department</th>
                          <th>Board</th>
                          <th>Result</th>
                          <th>Option</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <header class="sub-panel-heading" style="background-color: #e9eff1">
                  <h2 class="sub-panel-title">Professional Experience</h2>
                </header>
                <div class="row" style="width: 100%;">
                  <div class="col-sm-12" style="text-align: center; margin-bottom: 30px;">
                    <p>
                      <a class="btn btn-success btn-md" data-toggle="collapse" href="#experience_info" role="button" aria-expanded="false" aria-controls="experience_info" style="width: 100%;">
                        <i class="mdi mdi-plus-circle-outline"></i> ADD
                      </a>
                    </p>
                    <div class="collapse" id="experience_info">
                      <div class="card card-body">
                        <div class="row">

                          <div class="col-sm-12">
                            <div class="form-group">
                              <label class="control-label">Employer Name<span class="text-danger">*</span></label>
                              <input type="text" name="employer" placeholder="Employer Name" autocomplete="off" required
                                     value="{{ old('employer') }}"
                                     class="form-control @error('employer') is-invalid @enderror">
                              @error('employer')
                              <strong class="text-danger">{{ $errors->first('employer') }}</strong>
                              @enderror
                            </div>
                          </div>
                        </div>
                        <div class="row">

                          <div class="col-sm-4">
                            <div class="form-group">
                              <label class="control-label">Staring Position<span class="text-danger">*</span></label>
                              <input type="text" name="starting_position" placeholder="Staring Position" autocomplete="off" required
                                     value="{{ old('starting_position') }}"
                                     class="form-control @error('starting_position') is-invalid @enderror">
                              @error('starting_position')
                              <strong class="text-danger">{{ $errors->first('starting_position') }}</strong>
                              @enderror
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label class="control-label">Current/Last Position<span class="text-danger">*</span></label>
                              <input type="text" name="current_position" placeholder="Current/Last Position" autocomplete="off" required
                                     value="{{ old('current_position') }}"
                                     class="form-control @error('current_position') is-invalid @enderror">
                              @error('current_position')
                              <strong class="text-danger">{{ $errors->first('current_position') }}</strong>
                              @enderror
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label class="control-label">Position<span class="text-danger">*</span></label>
                              <input type="text" name="subject" placeholder="Subject/Department" autocomplete="off" required
                                     value="{{ old('subject') }}"
                                     class="form-control @error('subject') is-invalid @enderror">
                              @error('subject')
                              <strong class="text-danger">{{ $errors->first('subject') }}</strong>
                              @enderror
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label class="control-label">Job Start Date<span class="text-danger">*</span></label>
                              <input type="date" name="job_start_date" placeholder="Job Start Date" autocomplete="off" required
                                     value="{{ old('job_start_date') }}"
                                     class="form-control @error('job_start_date') is-invalid @enderror">
                              @error('job_start_date')
                              <strong class="text-danger">{{ $errors->first('job_start_date') }}</strong>
                              @enderror
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label class="control-label">Job Location<span class="text-danger">*</span></label>
                              <input type="date" name="job_location" placeholder="Job Location" autocomplete="off" required
                                     value="{{ old('job_location') }}"
                                     class="form-control @error('job_location') is-invalid @enderror">
                              @error('job_location')
                              <strong class="text-danger">{{ $errors->first('job_location') }}</strong>
                              @enderror
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label class="control-label">Duration<span class="text-danger">*</span></label>
                              <div class="row">
                                <div class="col-md-4">
                                  <input type="text" name="duration_year" placeholder="Year" autocomplete="off" required
                                         value="{{ old('duration_year') }}"
                                         class="form-control @error('duration_year') is-invalid @enderror"></div>
                                <div class="col-md-4">
                                  <input type="text" name="duration" placeholder="Month" autocomplete="off" required
                                         value="{{ old('duration_month') }}"
                                         class="form-control @error('duration_month') is-invalid @enderror"></div>
                                <div class="col-md-4">
                                  <input type="text" name="duration" placeholder="Day" autocomplete="off" required
                                         value="{{ old('duration_day') }}"
                                         class="form-control @error('duration_day') is-invalid @enderror"></div>
                              </div>
                              @error('duration')
                              <strong class="text-danger">{{ $errors->first('duration') }}</strong>
                              @enderror
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label class="control-label">Job Responsibility</label>
                              <textarea type="text" id="job_responsibility" name="job_responsibility" autocomplete="off"
                                        value="{{ old('job_responsibility') }}"
                                        class="form-control @error('job_responsibility') is-invalid @enderror"></textarea>
                              @error('job_responsibility')
                              <strong class="text-danger">{{ $errors->first('job_responsibility') }}</strong>
                              @enderror
                            </div>
                          </div>

                        </div>
                        <div class="row">
                          <div class="col-sm-12 text-right">
                            <button class="btn btn-danger btn-sm" type="submit">Submit</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12">

                    <div class="row">
                      <table id="datatable-buttons" class="table table-striped dt-responsive nowrap"
                             cellspacing="0" width="100%" style="font-size: 14px; margin: 0px 0px 10px 10px;">
                        <thead>
                        <tr>
                          <th width="50">#</th>
                          <th>Degree</th>
                          <th>Institution</th>
                          <th>Department</th>
                          <th>Board</th>
                          <th>Result</th>
                          <th>Option</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
{{--                <header class="sub-panel-heading" style="background-color: #e9eff1">--}}
{{--                  <h2 class="sub-panel-title">References</h2>--}}
{{--                </header>--}}
{{--                <div class="row" style="width: 100%;">--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <label class="control-label">Name</label>--}}
{{--                      <input type="text" name="Name" placeholder="" autocomplete="off" required--}}
{{--                             value="{{ old('Name') }}"--}}
{{--                             class="form-control @error('Name') is-invalid @enderror">--}}
{{--                      @error('Name')--}}
{{--                      <strong class="text-danger">{{ $errors->first('Name') }}</strong>--}}
{{--                      @enderror--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <label class="control-label">Designation</label>--}}
{{--                      <input type="text" name="designation" placeholder="Designation" autocomplete="off" required--}}
{{--                             value="{{ old('designation') }}"--}}
{{--                             class="form-control @error('designation') is-invalid @enderror">--}}
{{--                      @error('designation')--}}
{{--                      <strong class="text-danger">{{ $errors->first('designation') }}</strong>--}}
{{--                      @enderror--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <label class="control-label">Contact Email</label>--}}
{{--                      <input type="text" name="contact_email" placeholder="Contact Email" autocomplete="off" required--}}
{{--                             value="{{ old('contact_email') }}"--}}
{{--                             class="form-control @error('contact_email') is-invalid @enderror">--}}
{{--                      @error('contact_email')--}}
{{--                      <strong class="text-danger">{{ $errors->first('contact_email') }}</strong>--}}
{{--                      @enderror--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                  <div class="col-sm-6">--}}
{{--                    <div class="form-group">--}}
{{--                      <label class="control-label">Mobile no</label>--}}
{{--                      <input type="text" name="mobile_no" placeholder="Mobile No" autocomplete="off" required--}}
{{--                             value="{{ old('mobile_no') }}"--}}
{{--                             class="form-control @error('mobile_no') is-invalid @enderror">--}}
{{--                      @error('mobile_no')--}}
{{--                      <strong class="text-danger">{{ $errors->first('mobile_no') }}</strong>--}}
{{--                      @enderror--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                </div>--}}
                <header class="sub-panel-heading" style="background-color: #e9eff1">
                  <h2 class="sub-panel-title">NFTVQF Informations</h2>
                </header>
                <div class="row" style="width: 100%;">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="control-label">Title</label>
                      <input type="text" name="title" placeholder="Title" autocomplete="off" required
                             value="{{ old('title') }}"
                             class="form-control @error('title') is-invalid @enderror">
                      @error('title')
                      <strong class="text-danger">{{ $errors->first('title') }}</strong>
                      @enderror
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="control-label">Level</label>
                      <select name="ntvqf_level" class="select2 form-control @error('ntvqf_level') is-invalid @enderror" style="width: 10px !important;">
                        <option value="">Choose an option</option>
                        <option value="">Level 1</option>
                        <option value="">Level 2</option>
                        <option value="">Level 3</option>
                        <option value="">Level 4</option>
                        <option value="">Level 5</option>
                        <option value="">Level 4</option>
                        <option value="">Level 7</option>
                        <option value="">Level 8</option>
                        <option value="">Level 9</option>
                        <option value="">Level 10</option>
                      </select>
                      @error('ntvqf_level')
                      <strong class="text-danger">{{ $errors->first('ntvqf_level') }}</strong>
                      @enderror
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="control-label">Certified By</label>
                      <select name="ntvqf_level" class="select2 form-control @error('ntvqf_level') is-invalid @enderror" style="width: 10px !important;">
                        <option value="">Choose an option</option>
                        <option value="">BTEB</option>
                        <option value="">NSDA</option>
                      </select>
                      @error('ntvqf_level')
                      <strong class="text-danger">{{ $errors->first('ntvqf_level') }}</strong>
                      @enderror
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="control-label">Date</label>
                      <input type="date" name="date" placeholder="Date" autocomplete="off" required
                             value="{{ old('date') }}"
                             class="form-control @error('date') is-invalid @enderror">
                      @error('date')
                      <strong class="text-danger">{{ $errors->first('date') }}</strong>
                      @enderror
                    </div>
                  </div>
                </div>
                <header class="sub-panel-heading" style="background-color: #e9eff1">
                  <h2 class="sub-panel-title">Other Informations</h2>
                </header>
                <div class="row" style="width: 100%;">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <div class="well well-sm text-center">
                        <div class="row">
                          <label class="control-label">Freedom Fighter</label>
                        </div>
                        <div class="btn-group" data-toggle="buttons">

                          <label class="btn btn-success active">
                            <input type="radio" name="freedom_fighter" id="freedom_fighter" value="yes" autocomplete="off" chacked>
                            <span class="glyphicon glyphicon-ok">Yes</span>
                          </label>
                          <label class="btn btn-danger">
                            <input type="radio" name="freedom_fighter" id="freedom_fighter" value="yes" autocomplete="off">
                            <span class="glyphicon glyphicon-ok">No</span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <div class="well well-sm text-center">
                        <div class="row">
                          <label class="control-label">Special able People</label>
                        </div>
                        <div class="btn-group" data-toggle="buttons">

                          <label class="btn btn-success active">
                            <input type="radio" name="freedom_fighter" id="freedom_fighter" value="yes" autocomplete="off" chacked>
                            <span class="glyphicon glyphicon-ok">Yes</span>
                          </label>
                          <label class="btn btn-danger">
                            <input type="radio" name="freedom_fighter" id="freedom_fighter" value="yes" autocomplete="off">
                            <span class="glyphicon glyphicon-ok">No</span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 text-right">
                    <button class="btn btn-danger btn-sm" type="submit">Submit</button>
                  </div>
                </div>
{{--              </form>--}}
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
  <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
  <script src="{{ asset('assets/admin/plugins/summernote/summernote-bs4.js') }}"></script>
  <script>
    $(document).ready(function () {
      $('.select2').select2()
      $(document).ready(function() {
        $('#job_responsibility').summernote({
          'height': 100,
        });
        $('#additional_requirements').summernote({
          'height': 100,
        });
        $('#job_context').summernote({
          'height': 100,
        });
        $('#educational_requirement').summernote({
          'height': 100,
        });
      });
    });
  </script>
@endsection
