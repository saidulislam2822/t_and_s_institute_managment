@extends('layout.site')

@section('stylesheet')
    <style>
        .btn-success {
            color: white;
        }
    </style>
@endsection
@section('content')
    <div class="institute_body_align">

        <div class="container institute_head_alignment">
            <h2 class="text-center" id="title">Trainee Registration Form</h2>
            <hr>
            @if (session()->has('status'))
                {!! session()->get('status') !!}
            @endif
            <x-registration-header />
            <form id="show_student" class="form-horizontal toggleForm" action="{{ route('register') }}" method="post">
                @csrf
                {{--                <div  class="row mb-3"> --}}
                {{--                    <div class="col-sm-12"> --}}
                {{--                        <div class="form-group"> --}}
                {{--                            <label class="control-label">Institute</label> --}}
                {{--                            <select name="institute_id" class="form-control @error('institute_id') is-invalid @enderror"> --}}
                {{--                                <option value="">Choose an institute</option> --}}
                {{--                                @foreach ($institutes as $institute) --}}
                {{--                                    <option value="{{ $institute->id }}" @selected($institute->id == old('institute_id'))>{{ $institute->name }}</option> --}}
                {{--                                @endforeach --}}
                {{--                            </select> --}}
                {{--                            @error('institute_id') --}}
                {{--                            <strong class="text-danger">{{ $errors->first('institute_id') }}</strong> --}}
                {{--                            @enderror --}}
                {{--                        </div> --}}
                {{--                    </div> --}}
                {{--                </div> --}}

                <div class="row mb-3 mt-4">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="nid">NID No<span class="text-danger">*</span></label>
                            <input type="number" name="nid" id="nid" placeholder="Enter Your NID Number"
                                autocomplete="off" class="form-control @error('nid') is-invalid @enderror"
                                value="{{ old('nid') }}">
                            <span class="spin"></span>
                            @error('nid')
                                <strong class="text-danger">{{ $errors->first('nid') }}</strong>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row mb-3 mt-4">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="course_name">Course Name<span class="text-danger">*</span></label>
                            <input type="text" name="course_name" id="course_name" placeholder="Enter Your Name"
                                autocomplete="off" class="form-control @error('course_name') is-invalid @enderror"
                                value="{{ old('course_name') }}">
                            <span class="spin"></span>
                            @error('course_name')
                                <strong class="text-danger">{{ $errors->first('course_name') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="batch">Batch<span class="text-danger">*</span></label>
                            <input type="text" name="batch" id="batch" placeholder="026" autocomplete="off"
                                class="form-control @error('batch') is-invalid @enderror" value="{{ old('batch') }}">
                            <span class="spin"></span>
                            @error('batch')
                                <strong class="text-danger">{{ $errors->first('batch') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="duration">Duration<span class="text-danger">*</span></label>
                            <input type="text" name="duration" id="duration"
                                placeholder="23 October, 2022 - 10 November, 2022" autocomplete="off"
                                class="form-control @error('duration') is-invalid @enderror" value="{{ old('duration') }}">
                            <span class="spin"></span>
                            @error('duration')
                                <strong class="text-danger">{{ $errors->first('duration') }}</strong>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="fee">Fee<span class="text-danger">*</span></label>
                            <input type="text" name="fee" id="fee" placeholder="26000" autocomplete="off"
                                class="form-control @error('fee') is-invalid @enderror" value="{{ old('fee') }}">
                            <span class="spin"></span>
                            @error('fee')
                                <strong class="text-danger">{{ $errors->first('fee') }}</strong>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row mt-5">
                    <h5>Participant Name</h5>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name_en">Name (English)<span class="text-danger">*</span></label>
                            <input type="text" name="name_en" id="name_en" placeholder="Enter Your Name"
                                autocomplete="off" class="form-control @error('name_en') is-invalid @enderror"
                                value="{{ old('name_en') }}">
                            <span class="spin"></span>
                            @error('name_en')
                                <strong class="text-danger">{{ $errors->first('name_en') }}</strong>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name_bn">Name (Bangle)<span class="text-danger"></span></label>
                            <input type="text" name="name_bn" id="name_bn" placeholder="Enter Your Name"
                                autocomplete="off" class="form-control @error('name_bn') is-invalid @enderror"
                                value="{{ old('name_bn') }}">
                            <span class="spin"></span>
                            @error('name_bn')
                                <strong class="text-danger">{{ $errors->first('name_bn') }}</strong>
                            @enderror
                        </div>
                    </div>

                </div>

                <div class="row mb-3">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="father_name">Father Name ( Optional )<span class="text-danger"></span></label>
                            <input type="text" name="father_name" id="father_name"
                                placeholder="Enter Your Father Name" autocomplete="off"
                                class="form-control @error('father_name') is-invalid @enderror"
                                value="{{ old('father_name') }}">
                            <span class="spin"></span>
                            @error('father_name')
                                <strong class="text-danger">{{ $errors->first('father_name') }}</strong>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="mother_name">Mother Name ( Optional )<span class="text-danger"></span></label>
                            <input type="text" name="mother_name" id="mother_name"
                                placeholder="Enter Your Mother Name" autocomplete="off"
                                class="form-control @error('mother_name') is-invalid @enderror"
                                value="{{ old('mother_name') }}">
                            <span class="spin"></span>
                            @error('mother_name')
                                <strong class="text-danger">{{ $errors->first('mother_name') }}</strong>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Nominationa Type</label>
                            <select name="nominationa_id"
                                class="form-control @error('nominationa_id') is-invalid @enderror">
                                <option value="">Choose an institute</option>
                                {{--                                @foreach ($institutes as $institute) --}}
                                <option>Official</option>
                                {{--                                @endforeach --}}
                            </select>
                            @error('nominationa_id')
                                <strong class="text-danger">{{ $errors->first('nominationa_id') }}</strong>
                            @enderror
                        </div>
                    </div>

                </div>


                <div class="row mt-5">
                    <h5>Permanent Address</h5>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label for="village">a) Village/Moholla/Area<span class="text-danger"></span></label>
                            <input type="text" name="village" id="village" placeholder="Enter Your Village"
                                autocomplete="off" class="form-control @error('village') is-invalid @enderror"
                                value="{{ old('village') }}">
                            <span class="spin"></span>
                            @error('village')
                                <strong class="text-danger">{{ $errors->first('village') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="post_office">b)Post Office<span class="text-danger"></span></label>
                            <input type="text" name="post_office" id="post_office"
                                placeholder="Enter Your Post Office" autocomplete="off"
                                class="form-control @error('post_office') is-invalid @enderror"
                                value="{{ old('post_office') }}">
                            <span class="spin"></span>
                            @error('post_office')
                                <strong class="text-danger">{{ $errors->first('post_office') }}</strong>
                            @enderror
                        </div>
                    </div>

                </div>

                <div class="row mb-3">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="police_station">c) Police Station<span class="text-danger"></span></label>
                            <input type="text" name="police_station" id="police_station"
                                placeholder="Enter Your Police Station" autocomplete="off"
                                class="form-control @error('police_station') is-invalid @enderror"
                                value="{{ old('police_station') }}">
                            <span class="spin"></span>
                            @error('police_station')
                                <strong class="text-danger">{{ $errors->first('police_station') }}</strong>
                            @enderror
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="upazila">d) Upazila<span class="text-danger"></span></label>
                            <input type="text" name="upazila" id="upazila" placeholder="Enter Your Upazila"
                                autocomplete="off" class="form-control @error('upazila') is-invalid @enderror"
                                value="{{ old('upazila') }}">
                            <span class="spin"></span>
                            @error('upazila')
                                <strong class="text-danger">{{ $errors->first('upazila') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="district">e) District<span class="text-danger"></span></label>
                            <input type="text" name="district" id="district" placeholder="Enter Your District"
                                autocomplete="off" class="form-control @error('district') is-invalid @enderror"
                                value="{{ old('district') }}">
                            <span class="spin"></span>
                            @error('district')
                                <strong class="text-danger">{{ $errors->first('district') }}</strong>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row mt-5">
                    <h5>Present Address</h5>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label for="village">a) Village/Moholla/Area<span class="text-danger"></span></label>
                            <input type="text" name="village" id="village" placeholder="Enter Your Village"
                                autocomplete="off" class="form-control @error('village') is-invalid @enderror"
                                value="{{ old('village') }}">
                            <span class="spin"></span>
                            @error('village')
                                <strong class="text-danger">{{ $errors->first('village') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="post_office">b)Post Office<span class="text-danger"></span></label>
                            <input type="text" name="post_office" id="post_office"
                                placeholder="Enter Your Post Office" autocomplete="off"
                                class="form-control @error('post_office') is-invalid @enderror"
                                value="{{ old('post_office') }}">
                            <span class="spin"></span>
                            @error('post_office')
                                <strong class="text-danger">{{ $errors->first('post_office') }}</strong>
                            @enderror
                        </div>
                    </div>

                </div>

                <div class="row mb-3">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="post_code">b) Post Code<span class="text-danger"></span></label>
                            <input type="text" name="post_code" id="post_code" placeholder="Enter Your Post Code"
                                autocomplete="off" class="form-control @error('post_code') is-invalid @enderror"
                                value="{{ old('post_code') }}">
                            <span class="spin"></span>
                            @error('post_code')
                                <strong class="text-danger">{{ $errors->first('post_code') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="police_station">c) Police Station<span class="text-danger"></span></label>
                            <input type="text" name="police_station" id="police_station"
                                placeholder="Enter Your Police Station" autocomplete="off"
                                class="form-control @error('police_station') is-invalid @enderror"
                                value="{{ old('police_station') }}">
                            <span class="spin"></span>
                            @error('police_station')
                                <strong class="text-danger">{{ $errors->first('police_station') }}</strong>
                            @enderror
                        </div>
                    </div>


                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="upazila">d) Upazila<span class="text-danger"></span></label>
                            <input type="text" name="upazila" id="upazila" placeholder="Enter Your Upazila"
                                autocomplete="off" class="form-control @error('upazila') is-invalid @enderror"
                                value="{{ old('upazila') }}">
                            <span class="spin"></span>
                            @error('upazila')
                                <strong class="text-danger">{{ $errors->first('upazila') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="district">e) District<span class="text-danger"></span></label>
                            <input type="text" name="district" id="district" placeholder="Enter Your District"
                                autocomplete="off" class="form-control @error('district') is-invalid @enderror"
                                value="{{ old('district') }}">
                            <span class="spin"></span>
                            @error('district')
                                <strong class="text-danger">{{ $errors->first('district') }}</strong>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email <span class="text-danger">*</span></label>
                            <input type="email" name="email" placeholder="Email" value="{{ old('email') }}"
                                class="form-control @error('email') is-invalid @enderror" required>
                            @error('email')
                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="blood_group">Blood Group<span class="text-danger"></span></label>
                            <input type="text" name="blood_group" id="blood_group"
                                placeholder="Enter Your Blood Group" autocomplete="off"
                                class="form-control @error('blood_group') is-invalid @enderror"
                                value="{{ old('blood_group') }}">
                            <span class="spin"></span>
                            @error('blood_group')
                                <strong class="text-danger">{{ $errors->first('blood_group') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="mobile">Mobile Number<span class="text-danger"></span></label>
                            <input type="number" name="mobile" id="mobile" placeholder="088XXXXXXXXX"
                                autocomplete="off" class="form-control @error('mobile') is-invalid @enderror"
                                value="{{ old('mobile') }}">
                            <span class="spin"></span>
                            @error('mobile')
                                <strong class="text-danger">{{ $errors->first('mobile') }}</strong>
                            @enderror
                        </div>
                    </div>


                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="gender">Gender<span class="text-danger">*</span></label>
                            <input type="number" name="gender" placeholder="Enter Your Gender"
                                value="{{ old('gender') }}" class="form-control @error('gender') is-invalid @enderror"
                                required>
                            @error('gender')
                                <strong class="text-danger">{{ $errors->first('gender') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="birth">Birth Date<span class="text-danger"></span></label>
                            <input type="text" name="birth" id="birth" placeholder="Enter Your Birth Date"
                                autocomplete="off" class="form-control @error('birth') is-invalid @enderror"
                                value="{{ old('birth') }}">
                            <span class="spin"></span>
                            @error('birth')
                                <strong class="text-danger">{{ $errors->first('birth') }}</strong>
                            @enderror
                        </div>
                    </div>
                </div>


                <div class="row mt-5">
                    <h5>Organization Information*</h5>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="designation">a) Designation<span class="text-danger">*</span></label>
                            <input type="text" name="designation" id="designation"
                                placeholder="Enter Your Designation" autocomplete="off"
                                class="form-control @error('designation') is-invalid @enderror"
                                value="{{ old('designation') }}">
                            <span class="spin"></span>
                            @error('designation')
                                <strong class="text-danger">{{ $errors->first('designation') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="organization">b) Organization Name<span class="text-danger">*</span></label>
                            <input type="text" name="organization" id="organization"
                                placeholder="Enter Your Organization Name" autocomplete="off"
                                class="form-control @error('organization') is-invalid @enderror"
                                value="{{ old('organization') }}">
                            <span class="spin"></span>
                            @error('organization')
                                <strong class="text-danger">{{ $errors->first('organization') }}</strong>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="join_date">c) Join Date<span class="text-danger"></span></label>
                            <input type="text" name="join_date" id="join_date" placeholder="Enter Your Join Date"
                                autocomplete="off" class="form-control @error('join_date') is-invalid @enderror"
                                value="{{ old('join_date') }}">
                            <span class="spin"></span>
                            @error('join_date')
                                <strong class="text-danger">{{ $errors->first('join_date') }}</strong>
                            @enderror
                        </div>
                    </div>

                </div>


                <div class="row mb-3">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="telephone">d) Telephone<span class="text-danger"></span></label>
                            <input type="text" name="telephone" id="telephone" placeholder="Enter Your Telephone"
                                autocomplete="off" class="form-control @error('telephone') is-invalid @enderror"
                                value="{{ old('telephone') }}">
                            <span class="spin"></span>
                            @error('telephone')
                                <strong class="text-danger">{{ $errors->first('telephone') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="fax">e) Fax<span class="text-danger"></span></label>
                            <input type="text" name="fax" id="fax" placeholder="Enter Your Fax"
                                autocomplete="off" class="form-control @error('fax') is-invalid @enderror"
                                value="{{ old('fax') }}">
                            <span class="spin"></span>
                            @error('fax')
                                <strong class="text-danger">{{ $errors->first('fax') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="mobile">f) Mobile<span class="text-danger"></span></label>
                            <input type="text" name="mobile" id="mobile" placeholder="Enter Your Mobile"
                                autocomplete="off" class="form-control @error('mobile') is-invalid @enderror"
                                value="{{ old('mobile') }}">
                            <span class="spin"></span>
                            @error('mobile')
                                <strong class="text-danger">{{ $errors->first('mobile') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="email">g) Email<span class="text-danger"></span></label>
                            <input type="text" name="email" id="email" placeholder="Enter Your Join Date"
                                autocomplete="off" class="form-control @error('email') is-invalid @enderror"
                                value="{{ old('email') }}">
                            <span class="spin"></span>
                            @error('email')
                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">h) Organization Type*</label>
                            <select name="organization_type"
                                class="form-control @error('organization_type') is-invalid @enderror">
                                <option value="">Choose an institute</option>
                                {{--                                @foreach ($institutes as $institute) --}}
                                <option>Official</option>
                                {{--                                @endforeach --}}
                            </select>
                            @error('organization_type')
                                <strong class="text-danger">{{ $errors->first('organization_type') }}</strong>
                            @enderror
                        </div>
                    </div>


                </div>

                <div class="row mt-5">
                    <h5>Name of the Degree (Last)*</h5>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="higher_degree">Highest Degree Name<span class="text-danger">*</span></label>
                            <input type="text" name="higher_degree" id="higher_degree"
                                placeholder="Highest Degree Name" autocomplete="off"
                                class="form-control @error('higher_degree') is-invalid @enderror"
                                value="{{ old('higher_degree') }}">
                            <span class="spin"></span>
                            @error('higher_degree')
                                <strong class="text-danger">{{ $errors->first('higher_degree') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="major_sub">Major Subject<span class="text-danger">*</span></label>
                            <input type="text" name="major_sub" id="major_sub" placeholder="Major Subject"
                                autocomplete="off" class="form-control @error('major_sub') is-invalid @enderror"
                                value="{{ old('major_sub') }}">
                            <span class="spin"></span>
                            @error('major_sub')
                                <strong class="text-danger">{{ $errors->first('major_sub') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="certificate">Certificate Awared Year<span class="text-danger">*</span></label>
                            <input type="text" name="certificate" id="certificate"
                                placeholder="Certificate Awared Year" autocomplete="off"
                                class="form-control @error('certificate') is-invalid @enderror"
                                value="{{ old('certificate') }}">
                            <span class="spin"></span>
                            @error('certificate')
                                <strong class="text-danger">{{ $errors->first('certificate') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="university">University/Institute/Board<span class="text-danger">*</span></label>
                            <input type="text" name="university" id="university" placeholder="Enter Your University"
                                autocomplete="off" class="form-control @error('university') is-invalid @enderror"
                                value="{{ old('university') }}">
                            <span class="spin"></span>
                            @error('university')
                                <strong class="text-danger">{{ $errors->first('university') }}</strong>
                            @enderror
                        </div>
                    </div>

                </div>

                <div class="row mb-3">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="relared">Relared Training<span class="text-danger"></span></label>
                            <input type="text" name="relared" id="relared" placeholder="Relared Training"
                                autocomplete="off" class="form-control @error('relared') is-invalid @enderror"
                                value="{{ old('relared') }}">
                            <span class="spin"></span>
                            @error('relared')
                                <strong class="text-danger">{{ $errors->first('relared') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="food">Food Restriction (if any)<span class="text-danger"></span></label>
                            <input type="text" name="food" id="food" placeholder="Food Restriction"
                                autocomplete="off" class="form-control @error('food') is-invalid @enderror"
                                value="{{ old('food') }}">
                            <span class="spin"></span>
                            @error('food')
                                <strong class="text-danger">{{ $errors->first('food') }}</strong>
                            @enderror
                        </div>
                    </div>
                </div>


                <div class="row mt-5">
                    <h5>Assessment Information*</h5>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="name">NSC Level<span class="text-danger">*</span></label>
                            <input type="text" name="name" id="name" placeholder="NSC Level"
                                autocomplete="off" class="form-control @error('name') is-invalid @enderror"
                                value="{{ old('name') }}">
                            <span class="spin"></span>
                            @error('name')
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="name">CB T&A <span class="text-danger">*</span></label>
                            <input type="text" name="name" id="name" placeholder="CB T&A "
                                autocomplete="off" class="form-control @error('name') is-invalid @enderror"
                                value="{{ old('name') }}">
                            <span class="spin"></span>
                            @error('name')
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="name">Assessment Type<span class="text-danger">*</span></label>
                            <input type="text" name="name" id="name" placeholder="Assessment Type"
                                autocomplete="off" class="form-control @error('name') is-invalid @enderror"
                                value="{{ old('name') }}">
                            <span class="spin"></span>
                            @error('name')
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="name">Candidate type <span class="text-danger">*</span></label>
                            <input type="text" name="name" id="name" placeholder="Candidate type"
                                autocomplete="off" class="form-control @error('name') is-invalid @enderror"
                                value="{{ old('name') }}">
                            <span class="spin"></span>
                            @error('name')
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            @enderror
                        </div>
                    </div>
                </div>



                <div class="row mt-5">
                    <h5>Emergency Contact Information*</h5>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="name">a) Name<span class="text-danger">*</span></label>
                            <input type="text" name="name" id="name" placeholder="Enter Your Name"
                                autocomplete="off" class="form-control @error('name') is-invalid @enderror"
                                value="{{ old('name') }}">
                            <span class="spin"></span>
                            @error('name')
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="address">b) Address<span class="text-danger"></span></label>
                            <input type="text" name="address" id="address" placeholder="Enter Your Address"
                                autocomplete="off" class="form-control @error('address') is-invalid @enderror"
                                value="{{ old('address') }}">
                            <span class="spin"></span>
                            @error('address')
                                <strong class="text-danger">{{ $errors->first('address') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="mobile">c) Mobile/ Telephone<span class="text-danger">*</span></label>
                            <input type="text" name="mobile" id="mobile" placeholder="Enter Your Mobile"
                                autocomplete="off" class="form-control @error('mobile') is-invalid @enderror"
                                value="{{ old('mobile') }}">
                            <span class="spin"></span>
                            @error('mobile')
                                <strong class="text-danger">{{ $errors->first('mobile') }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="relationship">d) Relationship with participant<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="relationship" id="relationship"
                                placeholder="Enter Your Relationship" autocomplete="off"
                                class="form-control @error('relationship') is-invalid @enderror"
                                value="{{ old('relationship') }}">
                            <span class="spin"></span>
                            @error('relationship')
                                <strong class="text-danger">{{ $errors->first('relationship') }}</strong>
                            @enderror
                        </div>
                    </div>

                </div>


                <div class="row mb-5">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="photo">Photo(Upload)<span class="text-danger"></span></label>
                            <input type="file" name="photo" id="photo" placeholder="Highest Degree Name"
                                autocomplete="off" class="form-control @error('photo') is-invalid @enderror"
                                value="{{ old('photo') }}">
                            <span class="spin"></span>
                            @error('hoto')
                                <strong class="text-danger">{{ $errors->first('photo') }}</strong>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="photo">Signature(Upload)<span class="text-danger"></span></label>
                            <input type="file" name="photo" id="photo" placeholder="Highest Degree Name"
                                autocomplete="off" class="form-control @error('photo') is-invalid @enderror"
                                value="{{ old('photo') }}">
                            <span class="spin"></span>
                            @error('hoto')
                                <strong class="text-danger">{{ $errors->first('photo') }}</strong>
                            @enderror
                        </div>
                    </div>

                </div>
                <div style="text-align: center" class="row mt-3">
                    <div class="text-right d-flex justify-content-center">
                        <button class="btn btn-success text-center w-full" type="submit">Register</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection

@section('script')
@endsection
