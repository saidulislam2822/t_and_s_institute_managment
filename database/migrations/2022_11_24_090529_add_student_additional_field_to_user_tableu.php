<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('institute_type')->nullable();
            $table->string('shift')->nullable();
            $table->string('section')->nullable();
            $table->string('department')->nullable();
            $table->string('semester')->nullable();
            $table->string('year')->nullable();
            $table->string('session')->nullable();
            $table->string('board_roll')->nullable();
            $table->string('running_board_roll')->nullable();
            $table->string('admission_year')->nullable();
            $table->string('birth_certificate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('institute_type');
            $table->dropColumn('department');
            $table->dropColumn('semester');
            $table->dropColumn('year');
            $table->dropColumn('board_roll');
            $table->dropColumn('session');
        });
    }
};
