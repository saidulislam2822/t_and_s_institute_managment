@extends('layout.site')

@section('stylesheet')
  <!-- DataTables -->
  <link href="{{ asset('assets/admin/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css"/>
  <link href="{{ asset('assets/admin/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css"/>

  <link href="{{ asset('assets/admin/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet"
        type="text/css"/>

@endsection

@section('content')

  <section id="services" class="services section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title mt-5">
        <h2 style="text-align: center">Job List</h2>

      </div>
      <div class="row">
        <div class="col-sm-10">
        </div>
      </div>

      <div class="card job-card">
{{--        <div style="margin-bottom: 20px;" class="row">--}}
{{--          --}}{{--              <div class="col-md-10">--}}
{{--          --}}{{--                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, sed.</p>--}}
{{--          --}}{{--              </div>--}}
{{--          --}}{{--              <div class="col-md-1 float-right">--}}
{{--          --}}{{--                <select name="" id="" class="form-control">--}}
{{--          --}}{{--                  <option value="">10</option>--}}
{{--          --}}{{--                  <option value="">20</option>--}}
{{--          --}}{{--                  <option value="">30</option>--}}
{{--          --}}{{--                </select>--}}
{{--          --}}{{--              </div>--}}
{{--        </div>--}}
        <div class="row" >
          <div class="col-md-2" style="background-color: #404040">
{{--            <ul>--}}
{{--              <li>Lorem ipsum dolor sit amet.</li>--}}
{{--              <li>Lorem ipsum dolor sit amet.</li>--}}
{{--              <li>Lorem ipsum dolor sit amet.</li>--}}
{{--              <li>Lorem ipsum dolor sit amet.</li>--}}
{{--            </ul>--}}
          </div>
          <div class="col-md-1">

          </div>

          <div class="col-md-9">
            <div class="row job-list">
{{--              <div class="col-md-2"></div>--}}
              <div class="col-md-12 job-overview">
                <div class="card jobs">
                  <div class="card-body">
                    <div class="row  company_logo">
                      <div class="col-md-12">
                        <img src="{{ asset('assets/admin/images/logo-sm.png') }}" alt="">
                      </div>
                    </div>
                    <h4><a href="{{ route('job.application') }}">Senior Manager- Business Operations</a></h4>
                    <h7><a href="{{ route('job.application') }}">A Renowned Group of Companies</a></h7>
                    <div class="short-overview">
                      <P><i class="bx bx-current-location"></i>Address </P>
                      <P><i class="bx bxs-school"></i>University</P>
                      <P><i class="bx bx-accessibility"></i>experience</P>
                    </div>
                    <div style="float: right;">
                      <p><i class="bx bi-watch"></i>Deadline: <strong style="font-size: 14px;">31 Jan 2023</strong>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row job-list">
              {{--              <div class="col-md-2"></div>--}}
              <div class="col-md-12 job-overview">
                <div class="card jobs">
                  <div class="card-body">
                    <div class="row  company_logo">
                      <div class="col-md-12">
                        <img src="{{ asset('assets/admin/images/logo-sm.png') }}" alt="">
                      </div>
                    </div>
                    <h4><a href="{{ route('job.application') }}">Senior Manager- Business Operations</a></h4>
                    <h7><a href="{{ route('job.application') }}">A Renowned Group of Companies</a></h7>
                    <div class="short-overview">
                      <P><i class="bx bx-current-location"></i>Address </P>
                      <P><i class="bx bxs-school"></i>University</P>
                      <P><i class="bx bx-accessibility"></i>experience</P>
                    </div>
                    <div style="float: right;">
                      <p><i class="bx bi-watch"></i>Deadline: <strong style="font-size: 14px;">31 Jan 2023</strong>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div style="margin-bottom: 20px;" class="row">

          <div class="col-md-12">
            <nav aria-label="Page navigation example" style="float: right !important; margin-right: 30px;">
              <ul class="pagination ">
                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">Next</a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>

      <style>
          .job-card {
              padding-top: 12px;
              padding-left: 30px;
              padding-right: 30px;
          }

          .job-list {
              margin-top: 25px;
              margin-bottom: 50px;
          }

          .job-overview {
              /*margin: -14px 0px -14px 38px !important;*/
          }

          .short-overview {
              line-height: 3px;
              margin-top: 30px;
          }

          .short-overview i {
              margin-right: 5px;
          }

          .company_logo {
              margin: 14px -75px 0px 0px !important;
              text-align: right;
              float: right !important;
          }

          .company_logo img {
              width: 100px !important;
          }

          .jobs {
              border-radius: 4px;
              background: #f5f5f5;
              box-shadow: 0 6px 10px rgba(0, 0, 0, .08), 0 0 6px rgba(0, 0, 0, .05);
              transition: .5s transform cubic-bezier(.155, 1.105, .295, 1.12), .3s box-shadow, .5s -webkit-transform cubic-bezier(.155, 1.105, .295, 1.12);
              padding: 14px 80px 18px 36px;
              cursor: pointer;
          }

          .jobs:hover {
              transform: scale(1.01);
              box-shadow: 0 10px 20px rgba(0, 0, 0, .12), 0 4px 8px rgba(0, 0, 0, .06);
          }

      </style>
    </div>
  </section><!-- End Services Section -->
@endsection



