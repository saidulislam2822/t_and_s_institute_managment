@extends('layout.site')

@section('stylesheet')
  <style>
      .form-sub-section{
          background-color: rgba(25, 135, 84, 0.726) !important;
          /* color: black !important; */
      }
  </style>
@endsection
@section('content')
  <div class="institute_body_align">

    <div class="container institute_head_alignment">
      <h2 class="text-center" id="title">Trainee Registration Form</h2>
      <hr>
      @if(session()->has('status'))
        {!! session()->get('status') !!}
      @endif
      <x-registration-header />
      <form id="show_student" class="form-horizontal toggleForm" action="{{ route('trainee.registration')}}" enctype="multipart/form-data" method="post">
        @csrf
        <div  class="row mb-3 mt-3">
          <div class="col-md-4">
            <div class="form-group">
              <label for="nid">NID</label>
              <input type="number" name="nid" id="nid" placeholder="Enter Your NID Number" autocomplete="off"
                     class="form-control @error('nid') is-invalid @enderror" value="{{ old('nid') }}">
              <span class="spin"></span>
              @error('nid')
              <strong class="text-danger">{{ $errors->first('nid') }}</strong>
              @enderror
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="full_name_en">Full name[English]<span class="text-danger">*</span></label>
              <input type="text" name="full_name_en" id="full_name_en" placeholder="Enter Full name in English" autocomplete="off"
                     class="form-control @error('full_name_en') is-invalid @enderror" value="{{ old('full_name_en') }}">
              <span class="spin"></span>
              @error('full_name_en')
              <strong class="text-danger">{{ $errors->first('full_name_en') }}</strong>
              @enderror
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="full_name_bn">Full Name[Bangla]<span class="text-danger">*</span></label>
              <input type="text" name="full_name_bn" id="full_name_bn" placeholder="Enter Full name in Bangla" autocomplete="off"
                     class="form-control @error('full_name_bn') is-invalid @enderror" value="{{ old('full_name_bn') }}">
              <span class="spin"></span>
              @error('full_name_bn')
              <strong class="text-danger">{{ $errors->first('full_name_bn') }}</strong>
              @enderror
            </div>
          </div>
        </div>
        <div class="row mb-3">
          <div class="col-md-9">

            <div class="row mb-3">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="office">Office<span class="text-danger">*</span></label>
                  <input type="text" name="office" id="office" placeholder="Enter Your Office Name" autocomplete="off"
                         class="form-control @error('office') is-invalid @enderror" value="{{ old('office') }}">
                  <span class="spin"></span>
                  @error('office')
                  <strong class="text-danger">{{ $errors->first('office') }}</strong>
                  @enderror
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <label class="control-label">Designation</label>
                  <select name="designation_id" class="form-control @error('designation_id') is-invalid @enderror">
                    <option value="">Choose a designation</option>
                    <option value="1">PD</option>
                    <option value="2">APD</option>
                    <option value="2">DPD</option>
                    <option value="2">Programmer</option>
                    <option value="2">PO</option>
                    <option value="2">AO</option>
                    <option value="2">ATO</option>
                  </select>
                  @error('designation_id')
                  <strong class="text-danger">{{ $errors->first('designation_id') }}</strong>
                  @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="phone">Mobile Number</label>
                  <input type="text" name="phone" id="phone" placeholder="Enter Your Mobile Number" autocomplete="off"
                         class="form-control @error('phone') is-invalid @enderror" value="{{ old('phone') }}">
                  <span class="spin"></span>
                  @error('phone')
                  <strong class="text-danger">{{ $errors->first('phone') }}</strong>
                  @enderror
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" name="email" id="email" placeholder="Enter Your Email" autocomplete="off"
                         class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                  <span class="spin"></span>
                  @error('email')
                  <strong class="text-danger">{{ $errors->first('email') }}</strong>
                  @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="username">Username</label>
                  <input type="text" name="username" id="username" placeholder="Enter Your Email" autocomplete="off"
                         class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}">
                  <span class="spin"></span>
                  @error('username')
                  <strong class="text-danger">{{ $errors->first('username') }}</strong>
                  @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="alt_phone">Phone No</label>
                  <input type="text" name="alt_phone" id="alt_phone" placeholder="Enter Your Phone Number" autocomplete="off"
                         class="form-control @error('alt_phone') is-invalid @enderror" value="{{ old('alt_phone') }}">
                  <span class="spin"></span>
                  @error('alt_phone')
                  <strong class="text-danger">{{ $errors->first('alt_phone') }}</strong>
                  @enderror
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" name="password" id="password" placeholder="Enter Your Password" autocomplete="off"
                         class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}">
                  <span class="spin"></span>
                  @error('password')
                  <strong class="text-danger">{{ $errors->first('password') }}</strong>
                  @enderror
                  <small id="realtime-password-error" class="text-danger d-none">Password must be at least one uppercase letter, one lowercase letter, one number and one special character </small>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="password_confirmation">Confirm Password</label>
                  <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Enter Your Password" autocomplete="off"
                         class="form-control @error('password_confirmation') is-invalid @enderror" value="{{ old('password_confirmation') }}">
                  <span class="spin"></span>
                  @error('password_confirmation')
                  <strong class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
                  @enderror
                  <small id="confirm-password-error" class="text-danger d-none">Enter the correct password</small>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="row mb-3">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="image">Profile Photo</label>
                  <input type="file" name="image" id="" class="form-control @error('image') is-invalid @enderror" value="{{old('image')}}">
                  @error('image')
                  <strong class="text-danger">{{$error->first('image')}}</strong>
                  @enderror
                  <div class="img-holder mb-4 d-flex justify-content-center">
                    <img src="{{ asset('assets/placeholder.png') }}"
                         alt="example placeholder" style="width: 131px; margin-top: 26px;" />
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div style="text-align: center" class="row mt-3">
          <div class="text-right d-flex justify-content-center">
            <button class="btn btn-success text-center w-full" type="submit">Register</button>
          </div>
        </div>
      </form>

    </div>
  </div>
@endsection

@section('script')
  <script src="{{ asset('assets/admin/plugins/select2/js/select2.min.js') }}"></script>
  <script>
    $(document).ready(function() {
      $('.select2').select2()
      $(".tabClick").click(function(e) {
        $('.toggleForm').hide();
        $('#show_'+e.target.id).show()
        $('.tabClick').removeClass('btn-success')
        $(this).addClass('btn-success')
      });

      $(document).on('keyup','input[name="password"]',function(e){
      let password = e.target.value;
      // Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
      const regExp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
      // console.log('passRegex ', password)

      if(password.match(regExp)){
        $('#realtime-password-error').addClass('d-none');
      }
      else{
        $('#realtime-password-error').removeClass('d-none');
      }

    })

    // confirm password event handler
    $('input[name="password_confirmation"]').keyup(function(){
      const confirmPassword =  $('input[name="password_confirmation"]').val();
      const password =  $('input[name="password"]').val();
      let confirmPasswordError =  $('#confirm-password-error');

      if(confirmPassword != password){
        confirmPasswordError.removeClass('d-none')
      }
      else{
        confirmPasswordError.addClass('d-none')
      }
    });
    });
    //Image preview
    $('input[type="file"][name="image"]').on('change', function(){
      var img_path = $(this)[0].value;
      var img_holder = $('.img-holder');
      var extension = img_path.substring(img_path.lastIndexOf('.')+1).toLowerCase();
      if(extension == 'jpeg' || extension == 'jpg' || extension == 'png'){
        if(typeof(FileReader) != 'undefined'){
          img_holder.empty();
          var reader = new FileReader();
          reader.onload = function(e){
            $('<img/>',{'src':e.target.result,'class':'img-fluid','style':'max-width:100px;margin-bottom:10px; margin-top: 26px;'}).appendTo(img_holder);
          }
          img_holder.show();
          reader.readAsDataURL($(this)[0].files[0]);
        }else{
          $(img_holder).html('This browser does not support FileReader');
        }
      }else{
        $(img_holder).empty();
      }
    });

  </script>


@endsection

